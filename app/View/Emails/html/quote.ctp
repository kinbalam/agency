<div class="row-fluid">
<div class="span10 offset1 hero-unit">
    <div class="row-fluid">
        <div class="span5">
            <h2>Cotizaci&oacute;n Folio # <?= $quote['Quote']['id'] ?></h2>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span5">
            <h4>Datos del Cliente</h4>
            <p>Nombre: <strong><?=$quote['Quote']['client']?></strong></p>
            <p>Tel&eacute;fono: <strong><?=$quote['Quote']['phone']?></strong></p>
            <p>Email: <strong><?=$quote['Quote']['email']?></strong></p>
        </div>
        <div class="span7">
            <h4>Datos del Veh&iacute;culo</h4>
            <div class="row-fluid">
                <div class="span6">
                    <p>Marca: <strong><?=$quote['Vehicle']['brand']?></strong></p>
                </div>
                <div class="span6">
                    <p>Modelo: <strong><?=$quote['Vehicle']['model']?></strong></p>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span6">
                    <p>A&ntilde;o: <strong><?=$quote['Vehicle']['year']?></strong></p>
                </div>
                <div class="span6">
                    <p>Color: <strong><?=$quote['Vehicle']['color']?></strong></p>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span6">
                    <p>No Serie: <strong><?=$quote['Vehicle']['no_serie']?></strong></p>
                </div>
                <div class="span6">
                    <p>No Motor: <strong><?=$quote['Vehicle']['no_motor']?></strong></p>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <h4>Descripci&oacute;n del Financiamiento</h4>
    <div class="row-fluid">
        <div class="span4">
            <p>Precio De Lista: <strong><?=number_format($quote['Quote']['price'],2)?>MXN</strong></p>
        </div>
        <div class="span4">
            <p>Enganche: <strong><?=number_format($quote['Quote']['down_payment'],2)?>MXN (<?=number_format($quote['Quote']['down_payment']/$quote['Quote']['price']*100,2)?>%)</strong></p>
        </div>
        <div class="span4">
            <p>Inter&eacute;s: <strong><?=number_format($quote['Quote']['interest'],2)?>%</strong></p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <table class="table">
                <thead>
                <th>Plazo</th>
                <th>Letra Adelantada</th>
                <th>Mensualidad</th>
                <th>Mensualidad Con Impuestos</th>
                </thead>
                <tbody>
                <? foreach($quote['QuoteDet'] as $quote_det){?>
                    <tr>
                        <td><?=$quote_det['months']?></td>
                        <td class="text-right"><?=number_format($quote_det['capital_payment'],2)?> MXN</td>
                        <td class="text-right"><?=number_format($quote_det['normal_payment'],2)?> MXN</td>
                        <td class="text-right"><?=number_format($quote_det['total_payment'],2)?> MXN</td>
                    </tr>
                <? }?>

                </tbody>
            </table>
        </div>
    </div>


    <div class="modal-footer">
        <div class="row-fluid">
            <div class="span8">
            <form method="post" action="<?=$this->Html->url(array('controller'=>'quotes','action'=>'send_email','plugin'=>'pannel'))?>/<?=$quote['Quote']['id']?>" style="float:left;">
                <input type="input" name="email" placeholder="Enviar a email" style="float:left;margin-top: 2px;"/>
                <input type="submit" class="btn btn-primary" value="Enviar" style="float:left;margin-left: 5px;">
            </form>
            </div>
            <div class="span2">
                <a class="btn btn-danger" href="<?=$this->Html->url(array('controller'=>'quotes','action'=>'delete','plugin'=>'pannel'))?>/<?=$quote['Quote']['id']?>"><i class="icon-remove icon-white"></i> Dar de Baja</a>
            </div>
            <div class="span2">
                <a class="btn btn-primary" href="<?=$this->Html->url(array('controller'=>'quotes','action'=>'pdf','plugin'=>'pannel'))?>/<?=$quote['Quote']['id']?>" target="_blank"><i class="icon-print icon-white"></i> Imprimir</a>
            </div>
        </div>
        
        
        
    </div>
</div>
</div>