-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 17-02-2014 a las 21:35:18
-- Versión del servidor: 5.5.29
-- Versión de PHP: 5.4.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `agencia-project`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acos`
--

CREATE TABLE `acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  `_ismenu` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`, `_ismenu`) VALUES
(1, NULL, '', NULL, 'controllers', 1, 8, NULL),
(2, 1, '', NULL, 'Users', 2, 3, 1),
(3, 1, '', NULL, 'Vehicles', 4, 5, 1),
(4, 1, '', NULL, 'Quotes', 6, 7, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aros`
--

CREATE TABLE `aros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, '', NULL, 'admin', 1, 4),
(2, NULL, '', NULL, 'guest', 5, 6),
(3, 1, '', 20, 'system', 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aros_acos`
--

CREATE TABLE `aros_acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) unsigned NOT NULL,
  `aco_id` int(10) unsigned NOT NULL,
  `_create` char(2) NOT NULL DEFAULT '0',
  `_read` char(2) NOT NULL DEFAULT '0',
  `_update` char(2) NOT NULL DEFAULT '0',
  `_delete` char(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
(1, 1, 1, '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `quotes`
--

CREATE TABLE `quotes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client` varchar(200) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `down_payment` decimal(12,2) NOT NULL,
  `interest` decimal(12,2) NOT NULL,
  `tax` decimal(12,2) NOT NULL,
  `percentage` decimal(12,2) NOT NULL,
  `balance` decimal(12,2) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `quotes`
--

INSERT INTO `quotes` (`id`, `client`, `phone`, `email`, `price`, `down_payment`, `interest`, `tax`, `percentage`, `balance`, `vehicle_id`, `active`, `created`, `modified`) VALUES
(4, 'Miguel Bolivar', '525512041301', 'sales@asore.mx', 50000.00, 10000.00, 1.50, 16.00, 0.00, 0.00, 15, 1, '2014-02-05 06:41:12', '2014-02-05 06:41:12'),
(5, 'Miguel Bolivar', '1929192', 'm87.bolivar@gmail.com', 10000.00, 2000.00, 1.50, 16.00, 20.00, 0.00, 15, 1, '2014-02-05 06:45:41', '2014-02-05 06:45:41'),
(6, 'Samuel Balam', '98466556', 'caas@jhaaja.com', 120000.00, 24000.00, 1.50, 16.00, 20.00, 96000.00, 16, 1, '2014-02-05 06:47:04', '2014-02-05 06:47:04'),
(7, 'Miguel Bolivar', '9992919291', '12@ja.com', 12.00, 2.40, 1.50, 16.00, 20.00, 9.60, 15, 1, '2014-02-05 06:50:15', '2014-02-05 06:50:15'),
(8, 'Miguel Bolivar', '9992919291', '12@ja.com', 12.00, 2.40, 1.00, 1.00, 20.00, 9.60, 15, 1, '2014-02-05 06:53:48', '2014-02-05 06:53:48'),
(9, 'Jorge', '65665656', 'jaja@hahaha.com', 12.00, 2.40, 1.50, 16.00, 20.00, 9.60, 16, 1, '2014-02-05 14:50:30', '2014-02-05 14:50:30'),
(10, 'Luc', '1212121', 'lalala@jajaja.com', 12.00, 2.40, 1.50, 16.00, 20.00, 9.60, 18, 1, '2014-02-05 14:50:50', '2014-02-05 14:50:50'),
(11, 'Addy Cime', '232323', 'caas@jhaaja.com', 0.00, 0.00, 1.50, 16.00, 20.00, 0.00, 19, 1, '2014-02-05 14:51:13', '2014-02-05 14:51:13'),
(12, 'Miguel Bolivar', '9991910659', 'm87.bolivar@gmail.com', 100000.00, 20000.00, 1.50, 16.00, 20.00, 80000.00, 14, 1, '2014-02-12 06:48:37', '2014-02-12 06:48:37'),
(13, 'Cesar Iduarte', '99945554555', 'm87.bolivar@gmail.com', 50000.00, 10000.00, 1.50, 16.00, 20.00, 40000.00, 17, 1, '2014-02-15 11:50:28', '2014-02-15 11:50:28'),
(18, 'Cesar Iduarte', '99945454545', 'addy@cime.com', 50000.00, 10000.00, 1.50, 16.00, 20.00, 40000.00, 26, 1, '2014-02-15 18:03:30', '2014-02-15 18:03:30'),
(19, 'Cesar Iduarte', '99945454545', 'addy@cime.com', 50000.00, 10000.00, 1.50, 16.00, 20.00, 40000.00, 26, 1, '2014-02-15 18:04:07', '2014-02-15 18:04:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `quote_dets`
--

CREATE TABLE `quote_dets` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `months` int(11) NOT NULL,
  `capital_payment` decimal(12,2) NOT NULL,
  `normal_payment` decimal(12,2) NOT NULL,
  `total_payment` decimal(12,2) NOT NULL,
  `taxes` decimal(12,2) NOT NULL,
  `quote_id` bigint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `quote_id` (`quote_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Volcado de datos para la tabla `quote_dets`
--

INSERT INTO `quote_dets` (`id`, `months`, `capital_payment`, `normal_payment`, `total_payment`, `taxes`, `quote_id`) VALUES
(1, 12, 3333.33, 4696.00, 0.00, 0.00, 13),
(2, 45, 888.89, 1488.89, 0.00, 0.00, 13),
(3, 15, 2666.67, 3266.67, 0.00, 0.00, 13),
(4, 10, 4000.00, 4600.00, 0.00, 0.00, 13),
(9, 10, 4000.00, 4600.00, 4696.00, 0.00, 18),
(10, 20, 2000.00, 2600.00, 2696.00, 0.00, 18),
(11, 30, 1333.33, 1933.33, 2029.33, 0.00, 18),
(12, 40, 1000.00, 1600.00, 1696.00, 0.00, 18),
(13, 10, 4000.00, 4600.00, 4696.00, 0.00, 19),
(14, 20, 2000.00, 2600.00, 2696.00, 0.00, 19),
(15, 30, 1333.33, 1933.33, 2029.33, 0.00, 19),
(16, 40, 1000.00, 1600.00, 1696.00, 0.00, 19);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `description` varchar(25) DEFAULT NULL,
  `alias` varchar(30) DEFAULT NULL,
  `secret_code` char(40) NOT NULL,
  `auth_code` varchar(40) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `recovering` tinyint(1) DEFAULT NULL,
  `blocked` tinyint(1) DEFAULT NULL,
  `logged` bit(1) DEFAULT b'0',
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL,
  `accessed` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `description`, `alias`, `secret_code`, `auth_code`, `active`, `recovering`, `blocked`, `logged`, `created`, `modified`, `accessed`) VALUES
(20, 'system@gmail.com', 'system', 'ae39f61e08e5e329f67748f25f4ab12fe0412d40', NULL, 1, NULL, NULL, '', '2014-01-01', '2014-02-15', '2014-02-15'),
(21, 'empleado1', 'empleado1', 'ae39f61e08e5e329f67748f25f4ab12fe0412d40', '4153931afe0ba7fb4abaa93bbb2f1ddf3f8ab23a', 1, NULL, NULL, '\0', '2014-01-01', '2014-01-26', '2014-01-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `year` int(4) NOT NULL,
  `color` varchar(100) NOT NULL,
  `no_serie` varchar(100) NOT NULL,
  `no_motor` varchar(100) NOT NULL,
  `km` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Volcado de datos para la tabla `vehicles`
--

INSERT INTO `vehicles` (`id`, `brand`, `model`, `year`, `color`, `no_serie`, `no_motor`, `km`, `active`, `created`, `modified`) VALUES
(2, 'aaa', 'aaa', 100000, 'aaa', 'aaaa', 'aaaa', 1000, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'aaa', 'aaa', 100000, 'aaa', 'aaaa', 'aaaa', 5000, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Mazda', 'Mazda 3', 2013, 'Blanco', '45566565', '6565566556', 100000, 1, '2014-01-25 10:25:55', '2014-01-25 10:25:55'),
(15, 'Ford', 'Mazda 3', 2010, 'Amarillo', '654132323232', '323232323232', 1212, 1, '2014-01-25 10:27:03', '2014-01-25 10:27:03'),
(16, 'Honda', 'Civic', 1995, 'Azul', '35556566565', '2332233232', 12000, 1, '2014-01-25 10:34:51', '2014-01-25 10:34:51'),
(17, 'Jeep', 'Patriot', 2001, 'Blanco', '5665566', '565656', 112222, 1, '2014-01-25 10:35:20', '2014-01-25 10:35:20'),
(18, 'Nissan', 'Tsuru', 2011, 'Rojo', '2566565', '56655665', 56656565, 1, '2014-01-25 10:36:04', '2014-01-25 10:36:04'),
(19, 'Seat', 'Ibiza', 2011, 'Amarillo', '65656565', '5656656565', 12000, 1, '2014-01-25 10:39:16', '2014-01-25 10:39:16'),
(20, 'Volkwagen', 'Jetta', 2010, 'Blanco', '56565665', '5665656565', 12000, 1, '2014-01-25 10:40:04', '2014-01-25 10:40:04'),
(21, 'Volkwagen', 'Bora', 1998, 'Gris', '6656588998', '12121455', 200000, 1, '2014-01-25 10:40:49', '2014-01-25 10:40:49'),
(22, 'Ford', 'Expedition', 2014, 'Negro', '989664665', '6565665665', 500000, 1, '2014-01-25 10:41:48', '2014-01-25 10:41:48'),
(23, 'Seat', 'Cordoba', 2007, 'Verde', '956656565', '5232332', 50000, 1, '2014-01-25 10:42:26', '2014-01-25 10:42:26'),
(26, 'Jeep', 'Liberty', 2008, 'Magenta', '65568565656', '32296529', 52000, 1, '2014-01-25 10:46:45', '2014-01-25 10:46:45'),
(27, 'Dodge ', 'Aptitude', 2009, 'Blanco', '6958525623', '5669898', 24000, 1, '2014-01-25 10:48:51', '2014-01-25 10:48:51'),
(28, 'Chevrolet', 'Spark', 2009, 'Azul', '6565665', '56566565', 56656565, 1, '2014-01-25 10:49:41', '2014-01-25 10:49:41'),
(29, 'Mazda', '656565', 2009, 'Azul', '65565656656', '6565665656565', 2147483647, 1, '2014-01-25 11:21:38', '2014-01-25 11:21:38'),
(30, 'Jeep', 'Cherokee', 2009, 'Blanco', '56656565', '65656565', 12000, 1, '2014-01-25 11:25:54', '2014-01-25 11:25:54'),
(32, 'Mazda', 'Mazda 6', 2009, 'Azul', 'jasksalkjaslkklslk', 'klqlklkslksalksa', 100000000, 1, '2014-01-25 11:43:28', '2014-01-25 11:43:28'),
(33, 'Mazda', 'Mazda 7', 2013, 'Azul', '65656656565', '656565656565', 100000, 1, '2014-01-25 11:48:24', '2014-01-25 11:48:24');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `quote_dets`
--
ALTER TABLE `quote_dets`
  ADD CONSTRAINT `quote_dets_ibfk_1` FOREIGN KEY (`quote_id`) REFERENCES `quotes` (`id`);
