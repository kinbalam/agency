<?php
/**
 * Created by JetBrains PhpStorm.
 * User: miguel
 * Date: 05/09/13
 * Time: 10:00
 * To change this template use File | Settings | File Templates.
 */

class pdf_status extends FPDF{

    public $contract;

    public $wColumn;


    public function Header(){

        $border=1;

        $w = $this->w-$this->lMargin-$this->rMargin;


        $this->SetFont('Arial','B',24);
        $this->Cell($w,10,utf8_decode('Autos Durán - Contrato #').$this->contract['Contract']['id'],0,1);
        $this->SetFont('Arial','',8);
        $this->Cell($w,8,utf8_decode('Fecha de Creación ').$this->contract['Contract']['created_at'].'. '.utf8_decode('Fecha de Impresión ').date('Y-m-d H:i:s'),0,1);

        $x = $this->x;
        $y = $this->y;

        $this->Cell($w,35,null,1,1);

        $this->x = $x;
        $this->y = $y;

        $h = 3;
        $border=0;

        $this->SetFont('Arial','B',8);
        $this->Cell($w/2,$h,'Cliente',$border,0);
        $this->Cell($w/2,$h,'Aval',$border,1);

        $this->SetFont('Arial','',8);

        $this->Cell($w/2,$h,utf8_decode($this->contract['Contract']['client_name']),$border,0);
        $this->Cell($w/2,$h,utf8_decode($this->contract['Contract']['endorsement_name']),$border,1);

        $this->Cell($w/2,$h,utf8_decode($this->contract['Contract']['client_address']),$border,0);
        $this->Cell($w/2,$h,utf8_decode($this->contract['Contract']['endorsement_address']),$border,1);

        $this->Cell($w/2,$h,utf8_decode($this->contract['Contract']['client_city'].' '.$this->contract['Contract']['client_state']),$border,0);
        $this->Cell($w/2,$h,utf8_decode($this->contract['Contract']['endorsement_city'].' '.$this->contract['Contract']['endorsement_state']),$border,1);

        $this->Cell($w/2,$h,utf8_decode('Tel: '.$this->contract['Contract']['client_phone'].' Cel: '.$this->contract['Contract']['client_cellular']),$border,0);
        $this->Cell($w/2,$h,utf8_decode('Tel: '.$this->contract['Contract']['endorsement_phone'].' Cel: '.$this->contract['Contract']['endorsement_cellular']),$border,1);
        $this->ln(2);



        $this->SetFont('Arial','B',8);
        $this->Cell($w,$h,utf8_decode('Datos del Vehículo'),$border,1);

        $this->SetFont('Arial','B',8);
        $this->Cell($w/16,$h,utf8_decode('Marca:'),$border,0);
        $this->SetFont('Arial','',8);
        $this->Cell(3*$w/16,$h,utf8_decode($this->contract['Car']['marca']),$border,0);

        $this->SetFont('Arial','B',8);
        $this->Cell($w/16,$h,utf8_decode('Tipo:'),$border,0);
        $this->SetFont('Arial','',8);
        $this->Cell(3*$w/16,$h,utf8_decode($this->contract['Car']['tipo']),$border,0);

        $this->SetFont('Arial','B',8);
        $this->Cell($w/16,$h,utf8_decode('Modelo:'),$border,0);
        $this->SetFont('Arial','',8);
        $this->Cell(3*$w/16,$h,utf8_decode($this->contract['Car']['modelo']),$border,0);

        $this->SetFont('Arial','B',8);
        $this->Cell($w/16,$h,utf8_decode('Color:'),$border,0);
        $this->SetFont('Arial','',8);
        $this->Cell(3*$w/16,$h,utf8_decode($this->contract['Car']['color']),$border,1);
        $this->ln(2);

        $this->SetFont('Arial','B',8);
        $this->Cell(10*$w/15,$h,utf8_decode('Datos del Financiamiento'),$border,0);


        $this->SetFont('Arial','B',8);
        $this->Cell(2*$w/15+3,$h,utf8_decode('Pago Oportuno:'),$border,0);


        $this->SetFont('Arial','',8);
        $this->Cell(2*$w/15-3,$h,'$ '.number_format($this->contract['MonthlyPayment'][0]['capital']+$this->contract['MonthlyPayment'][0]['interest'],2),$border,1,'R');



        $this->SetFont('Arial','B',8);
        $this->Cell($w/15,$h,utf8_decode('Plazo:'),$border,0);
        $this->SetFont('Arial','',8);
        $this->Cell(4*$w/15,$h,number_format($this->contract['Contract']['months']),$border,0);

        $this->SetFont('Arial','B',8);
        $this->Cell(1*$w/15+3,$h,utf8_decode('Int. Mens.:'),$border,0);
        $this->SetFont('Arial','',8);
        $this->Cell(4*$w/15-3,$h,number_format($this->contract['Contract']['monthly_interest'],2).' %',$border,0);


        $this->SetFont('Arial','B',8);
        $this->Cell(2*$w/15+3,$h,utf8_decode('FMD:'),$border,0);
        $this->SetFont('Arial','',8);
        $this->Cell(2*$w/15-3,$h,'$ '.number_format($this->contract['Contract']['fmd'],2),$border,1,'R');




        $this->SetFont('Arial','B',8);
        $this->Cell($w/15,$h,utf8_decode('Precio: $'),$border,0);
        $this->SetFont('Arial','',8);
        $this->Cell(4*$w/15,$h,number_format($this->contract['Contract']['price'],2),$border,0);

        $this->SetFont('Arial','B',8);
        $this->Cell(1*$w/15+3,$h,utf8_decode('Engance: $'),$border,0);
        $this->SetFont('Arial','',8);
        $this->Cell(4*$w/15-3,$h,number_format($this->contract['Contract']['down_payment'],2),$border,0);

        $this->SetFont('Arial','B',8);
        $this->Cell(2*$w/15+3,$h,utf8_decode('Saldo a Financiar:'),$border,0);
        $this->SetFont('Arial','',8);
        $this->Cell(2*$w/15-3,$h,'$ '.number_format($this->contract['Contract']['price']-$this->contract['Contract']['down_payment'],2),$border,1,'R');
        $this->ln(5);


        //DIBU EL CONTENEDOR

        $x = $this->x;
        $y = $this->y;

        $this->x = $x;
        $this->y = $y;

        $h = 3;
        $border=0;

        $this->SetFont('Arial','B',8);

        $this->Cell($w,$this->h-$this->y-$this->bMargin,null,1,0);

        $this->x = $x;
        $this->Cell($w,$h,'',1,1);

        $this->x = $x;
        $this->y = $y;

        $this->SetFont('Arial','B',8);
        $this->Cell($w/10,$h,'   '.utf8_decode('Mes'),$border,0);
        $this->Cell($w/10,$h,utf8_decode('Vencimiento'),$border,0);
        $this->Cell($w/10,$h,utf8_decode('Estatus'),$border,0);
        $this->Cell($w/10,$h,utf8_decode('Capital'),$border,0);
        $this->Cell($w/10,$h,utf8_decode('Interés'),$border,0);
        $this->Cell($w/10,$h,utf8_decode('GC'),$border,0);
        $this->Cell($w/10,$h,utf8_decode('FMD'),$border,0);
        $this->Cell($w/10,$h,utf8_decode('Total'),$border,0);
        $this->Cell($w/10,$h,utf8_decode('Pagado'),$border,0);
        $this->Cell($w/10,$h,utf8_decode('Saldo'),$border,1);





    }

    public function Footer(){
        $this->y = $this->h- $this->bMargin  ;
        $w = $this->w-$this->lMargin-$this->rMargin;
        $this->SetFont('Arial','',9);
        $this->Cell($w,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,1);

    }

    /*!
  @function num2letras ()
  @abstract Dado un n?mero lo devuelve escrito.
  @param $num number - N?mero a convertir.
  @param $fem bool - Forma femenina (true) o no (false).
  @param $dec bool - Con decimales (true) o no (false).
  @result string - Devuelve el n?mero escrito en letra.

*/
    function num2letras($num, $fem = false, $dec = true) {
        $matuni[2]  = "dos";
        $matuni[3]  = "tres";
        $matuni[4]  = "cuatro";
        $matuni[5]  = "cinco";
        $matuni[6]  = "seis";
        $matuni[7]  = "siete";
        $matuni[8]  = "ocho";
        $matuni[9]  = "nueve";
        $matuni[10] = "diez";
        $matuni[11] = "once";
        $matuni[12] = "doce";
        $matuni[13] = "trece";
        $matuni[14] = "catorce";
        $matuni[15] = "quince";
        $matuni[16] = "dieciseis";
        $matuni[17] = "diecisiete";
        $matuni[18] = "dieciocho";
        $matuni[19] = "diecinueve";
        $matuni[20] = "veinte";
        $matunisub[2] = "dos";
        $matunisub[3] = "tres";
        $matunisub[4] = "cuatro";
        $matunisub[5] = "quin";
        $matunisub[6] = "seis";
        $matunisub[7] = "sete";
        $matunisub[8] = "ocho";
        $matunisub[9] = "nove";

        $matdec[2] = "veint";
        $matdec[3] = "treinta";
        $matdec[4] = "cuarenta";
        $matdec[5] = "cincuenta";
        $matdec[6] = "sesenta";
        $matdec[7] = "setenta";
        $matdec[8] = "ochenta";
        $matdec[9] = "noventa";
        $matsub[3]  = 'mill';
        $matsub[5]  = 'bill';
        $matsub[7]  = 'mill';
        $matsub[9]  = 'trill';
        $matsub[11] = 'mill';
        $matsub[13] = 'bill';
        $matsub[15] = 'mill';
        $matmil[4]  = 'millones';
        $matmil[6]  = 'billones';
        $matmil[7]  = 'de billones';
        $matmil[8]  = 'millones de billones';
        $matmil[10] = 'trillones';
        $matmil[11] = 'de trillones';
        $matmil[12] = 'millones de trillones';
        $matmil[13] = 'de trillones';
        $matmil[14] = 'billones de trillones';
        $matmil[15] = 'de billones de trillones';
        $matmil[16] = 'millones de billones de trillones';

        //Zi hack
        $float=explode('.',$num);
        $num=$float[0];

        $num = trim((string)@$num);
        if ($num[0] == '-') {
            $neg = 'menos ';
            $num = substr($num, 1);
        }else
            $neg = '';
        while ($num[0] == '0') $num = substr($num, 1);
        if ($num[0] < '1' or $num[0] > 9) $num = '0' . $num;
        $zeros = true;
        $punt = false;
        $ent = '';
        $fra = '';
        for ($c = 0; $c < strlen($num); $c++) {
            $n = $num[$c];
            if (! (strpos(".,'''", $n) === false)) {
                if ($punt) break;
                else{
                    $punt = true;
                    continue;
                }

            }elseif (! (strpos('0123456789', $n) === false)) {
                if ($punt) {
                    if ($n != '0') $zeros = false;
                    $fra .= $n;
                }else

                    $ent .= $n;
            }else

                break;

        }
        $ent = '     ' . $ent;
        if ($dec and $fra and ! $zeros) {
            $fin = ' coma';
            for ($n = 0; $n < strlen($fra); $n++) {
                if (($s = $fra[$n]) == '0')
                    $fin .= ' cero';
                elseif ($s == '1')
                    $fin .= $fem ? ' una' : ' un';
                else
                    $fin .= ' ' . $matuni[$s];
            }
        }else
            $fin = '';
        if ((int)$ent === 0) return 'Cero ' . $fin;
        $tex = '';
        $sub = 0;
        $mils = 0;
        $neutro = false;
        while ( ($num = substr($ent, -3)) != '   ') {
            $ent = substr($ent, 0, -3);
            if (++$sub < 3 and $fem) {
                $matuni[1] = 'una';
                $subcent = 'as';
            }else{
                $matuni[1] = $neutro ? 'un' : 'uno';
                $subcent = 'os';
            }
            $t = '';
            $n2 = substr($num, 1);
            if ($n2 == '00') {
            }elseif ($n2 < 21)
                $t = ' ' . $matuni[(int)$n2];
            elseif ($n2 < 30) {
                $n3 = $num[2];
                if ($n3 != 0) $t = 'i' . $matuni[$n3];
                $n2 = $num[1];
                $t = ' ' . $matdec[$n2] . $t;
            }else{
                $n3 = $num[2];
                if ($n3 != 0) $t = ' y ' . $matuni[$n3];
                $n2 = $num[1];
                $t = ' ' . $matdec[$n2] . $t;
            }
            $n = $num[0];
            if ($n == 1) {
                $t = ' ciento' . $t;
            }elseif ($n == 5){
                $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t;
            }elseif ($n != 0){
                $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t;
            }
            if ($sub == 1) {
            }elseif (! isset($matsub[$sub])) {
                if ($num == 1) {
                    $t = ' mil';
                }elseif ($num > 1){
                    $t .= ' mil';
                }
            }elseif ($num == 1) {
                $t .= ' ' . $matsub[$sub] . '?n';
            }elseif ($num > 1){
                $t .= ' ' . $matsub[$sub] . 'ones';
            }
            if ($num == '000') $mils ++;
            elseif ($mils != 0) {
                if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub];
                $mils = 0;
            }
            $neutro = true;
            $tex = $t . $tex;
        }
        $tex = $neg . substr($tex, 1) . $fin;
        //Zi hack --> return ucfirst($tex);
        $end_num=ucfirst($tex).' pesos '.$float[1].'/100 M.N.';
        return $end_num;
    }

}