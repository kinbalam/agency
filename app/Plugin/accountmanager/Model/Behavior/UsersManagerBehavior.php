<?php
class UsersManagerBehavior extends ModelBehavior
{
    protected $validate = array(
                'description' => array(
                    'validEntry' => array(
                        'rule' => 'notEmpty',
                        'message' => 'INVALID_USERNAME_EMAIL_EMPTY',
                        'last' => true
                    ),
                    'validMail' => array(
                        'rule' => 'email',
                        'message' => 'INVALID_USERNAME_EMAIL',
                        'last' => true
                    ),
                'isNew' => array(
                    'rule'=>'isValidDescription',
                    'message' => 'INVALID_USERNAME_EMAIL_TAKEN',
                    'on' => 'create',
                    'last' => true)
                ),
                'secret_code' => array(
                    'validEntry' => array(
                        'rule' => 'isValidPassword',
                        'message' => 'INVALID_PASSWORD',
                        'last' => true
                    )
                ),
                'confirm'=> array(
                    'validEntry' => array(
                        'rule' => 'isValidPassword',
                        'message' => 'INVALID_CONFIRMATION',
                        'last' => true
                    ),
                    'matchPassword' => array(
                        'rule' => 'isEqualToPassword',
                        'message' => 'PASSWORDS_MISMATCH',
                        'last' => true
                    )
                ),
                'alias' => array(
                    'validMaxLength' => array(
                        'rule' => array('maxLength', '30'),
                        'message' => 'INVALID_ALIAS_MAX_LENGTH',
                        'last' => true
                    ),
                    'validMinLength' => array(
                        'rule' => array('minLength', '5'),
                        'message' => 'INVALID_ALIAS_MIN_LENGTH',
                        'last' => true
                    ),
                    'validAlias' => array(
                        'rule' => 'isValidAlias',
                        'message' => 'ALIAS_TAKEN',
                        'last' => true
                    )
                ),
              );

    /**
     *
     * setup.
     *
     * Behavior callback method.
     * It is called when the behavior is attached to the model.
     * 
     * @param Model $model Reference to the model implementing the behavior.
     * @param array/mixed $settings behavior settings.
     *
     * @access public.
     * @return void.
     */
    public function setup(Model $model, $settings = array())
    {
        $model->validate = $this->validate;
    }

    /**
     *
     * isValidPassword.
     *
     * Custom validation method.
     *
     * @param array/mixed $check data to validate
     * @access public.
     * @return bool. 
     */
    public function isValidPassword($model, $check)
    {
        $emptyValue   = Configure::read('AccountManager.emptyPassword');
        $currentValue = array_shift($check);

        if($currentValue==$emptyValue || $currentValue=='')
        {
            return false;
        }
        
        return true;
    }

    /**
     *
     * isEqualToPassword.
     *
     * Custom validation method.
     *
     * @param array/mixed $check data to validate.
     * @access public.
     * @return bool if value passed is equal to account secret_code returns true, false otherwise.
     */
    public function isEqualToPassword($model, $check)
    {
        $value = array_shift($check);

        if($value!=$model->data[$model->name]['secret_code'])
            return false;
        
        return true;
    }

    /**
     *
     * isValidDescription.
     *
     * Custom validation method.
     *
     * @param array/mixed $check data to validate.
     * @access public.
     * @return bool if account's description does not exists returns true, false otherwise.
     */
    public function isValidDescription($model, $check)
    {
        $isValidDescription = false;
        $value = array_shift($check);
        $user  = $model->find('first',
                             array('fields' => array('id'),
                                   'conditions' => array('description'=>$value)
                                  )
                             );

        $isValidDescription = empty($user);

        if($model->id > 0 && !$isValidDescription)
        {
            if($model->id == $user[$model->name]['id'])
            {
                $isValidDescription = true;
            }
        }

        return $isValidDescription;
    }

    /**
     *
     * isValidAlias
     *
     * Custom validation method.
     * Validates if an alias is valid.
     *
     * @param object $model current model.
     * @param array/mixed $check data to validate.
     *
     * @return bool.
     */
    public function isValidAlias($model, $check)
    {
        $isValidAlias = false;
        $value = array_shift($check);
        $user = $model->find('first',
                             array(
                                'fields' => array('id'),
                                'conditions' => array('alias' => $value)
                                )
                            );

        $isValidAlias = empty($user);
        if($model->id > 0 && !$isValidAlias)
        {
            if($model->id == $user[$model->name]['id'])
            {
                $isValidAlias = true;
            }
        }

        return $isValidAlias;
    }

    /**
     *
     * logAccess.
     *
     * Updates account access flags.
     *
     * @param int $id account id.
     * @access public.
     * @return bool If updated returns true, false otherwise.
     */
    public function logAccess(&$model, $id)
    {
        if(!is_numeric($id))
            return false;

        $model->id = $id;
        $model->data['logged']   = true;
        $model->data['accessed'] = date(DATE_ATOM);

        return $model->save($model->data, false);
    }

    /**
     *
     * logout.
     *
     * Updates account logged field. 
     *
     * @param int $id account id.
     * @access public.
     * @return bool If saved returns true, false otherwise.
     */
    public function logout($model, $id)
    {
        $model->id = $id;
        return $model->saveField('logged', false);
    }
}
?>