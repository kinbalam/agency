<?php
class Account extends AccountManagerAppModel
{
    public $name             = 'Account';
    public $useTable         = 'users';
    public $actsAs           = array('AccountManager.UsersManager');
    public $validationDomain = 'account_manager';
}
?>