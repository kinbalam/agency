<?php
echo $this->Form->create('Account');
?>
    <h2><?php echo $title_for_layout;?></h2>
    <fieldset>
    <label><?php echo __d('account_manager', 'USERNAME_MAIL');?></label>
    <?php
    echo $this->Form->text('username', 
                           array('id' => 'username',
                                 'placeholder' => __d('account_manager', 'USERNAME_MAIL'))), 
         $this->Form->button(__d('account_manager', 'SEND_ACTIVATION_CODE_SUBMIT'), array('id' => 'send','type' => 'submit', 'class' => 'btn btn-large btn-primary'));
    ?>
    </fieldset>
<?php
echo $this->Form->end();
?>