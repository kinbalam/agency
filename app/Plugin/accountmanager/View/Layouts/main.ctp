<!-- DOCTYPE FOR HTML 5. -->
<!DOCTYPE html>
<html>
    <head>
        <!-- 
            Character Encoding.
            * Server Config (Transpor layer | Headers).
            * UBOM. (At the begining of the File):
            * <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">  | Still allowed.
        -->
        <meta charset="UTF-8">
        <title>Sistema de Control de Agencia</title>
        <?php echo $this->Html->css('/account_manager/js/bootstrap/css/bootstrap.min.css');?>
        <?php //echo $this->Html->css('/account_manager/css/style.css');?>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>
        <br/>
        <div class="navbar-wrapper">
            <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
            <div class="container">

                <div class="navbar navbar-inverse">
                    <div class="navbar-inner">
                        <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
                        <button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <? echo $this->Html->link('SCA | Autos Durán',
                                array( 'plugin' => 'pannel', 'controller' => 'users', 'action' => 'index' ),
                                array('class'=>'brand')); ?>
                        <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
                        <div class="nav-collapse collapse" style="height: 0px;">
                            <!-- Example Begin -->
                            <!--
                            <ul class="nav">
                                <li class="active"><a href="#">Home</a></li>
                                <li><a href="#about">About</a></li>
                                <li><a href="#contact">Contact</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li class="nav-header">Nav header</li>
                                        <li><a href="#">Separated link</a></li>
                                        <li><a href="#">One more separated link</a></li>
                                    </ul>
                                </li>
                            </ul>
                            -->
                            <!-- Example Ends -->
                        </div><!--/.nav-collapse -->
                    </div><!-- /.navbar-inner -->
                </div><!-- /.navbar -->

            </div> <!-- /.container -->
        </div>
        <div class="container">
            <div class="row">
                <?php
                $contentSpanClass = 'span12';
                ?>
                <div class="<?php echo $contentSpanClass;?>">
                    <div class="row-fluid">
                    <div class="span10 offset1">

                        <div class="row-fluid">
                            <div class="span12 text-center hero-unit">
                                <h1>Sistema de Control de Agencia</h1>
                                <p>Administra tu agencia de una manera f&aacute;cil y r&aacute;pida.</p>
                                <hr/>
                                <?php
                                echo $content_for_layout;
                                ?>
                            </div>      
                        </div>
                        <hr/>
                    </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <!--
        <footer>
            <div class="container">
                <?php
                if(isset($viewOptions))
                    echo $this->element('options', array('current' => $view));
                ?>
            </div>
        </footer>
        -->
        <?php
        if(isset($debug) && $debug)
            echo $this->element('sql_dump');
        
        echo $this->Html->script('/account_manager/js/jquery/jquery-1.9.1.js');
        echo $this->Html->script('/account_manager/js/bootstrap/js/bootstrap.min.js');
        ?>
        <script type="text/javascript">
            jQuery.noConflict();
        </script>
        <?php
        echo $scripts_for_layout;
        ?>
    </body>
</html>