<?php
App::uses('Component', 'Controller');
App::uses('Security','Utility');

class AccountServicesComponent extends Component
{
    public $errorMessage = '';

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    /**
     *
     * saveNewUser.
     *
     * 
     *
     * @param array/mixed $requestData user data from request object.
     * @access public.
     * @return bool true if success.
     */
    public function saveNewUser($requestData, $active=0)
    {
        $newUser = false;
        $modelName    = $this->controller->Account->name;
        $description  = $requestData['username'];

        if(isset($requestData['password_hash']))
        {
            $secret_code  = Security::hash($requestData['password_hash'], null, true);
            $confirm      = Security::hash($requestData['confirm_hash'], null, true);
        }

        unset($requestData['username']);
        unset($requestData['password_hash']);
        unset($requestData['confirm_hash']);
        
        $modelData[$modelName] = compact('description',
                                        'secret_code',
                                        'confirm',
                                        'active');
        
        foreach($requestData as $field=>$value)
        {
            $modelData[$modelName][$field] = $value;
        }

        if($this->controller->Account->save($modelData, true))
        {
            $newUser = true;
        }else
        {
            $error = current($this->controller->Account->validationErrors);
            $this->errorMessage = $error[0];
        }

        return $newUser;
    }

    /**
     *
     * createSelfRegisterUser.
     *
     * 
     *
     * @param array $requestData user data from request object.
     * @param string $authCode defines authetication code for active the account.
     * @param bool $active defines if the user is going to be active.
     * 
     * @access public.
     * @return string authentication code if success.
     */
    public function createSelfRegisterUser($requestData)
    {
        $authCode = '';

        if(!$this->isValidInput($requestData))
        {
            throw new BadFunctionCallException('Missing request data. The request must provide username, password_hash and confirm_hash values');
        }
        else
        {
            $description  = $requestData['username'];
            $secret_code  = Security::hash($requestData['password_hash'], null, true);
            $confirm      = Security::hash($requestData['confirm_hash'], null, true);
            $auth_code    = sha1(time());
            $active       = 0;

            $modelData['Account'] = compact('description',
                                            'secret_code',
                                            'confirm',
                                            'auth_code',
                                            'active'
                                            );

            if($this->controller->Account->save($modelData, true))
            {
                $authCode = $auth_code;
            }else
            {
                $error = current($this->controller->Account->validationErrors);
                $this->errorMessage = $error[0];
            }
        }

        return $authCode;
    }

    /**
     *
     * checkRequestData.
     *
     *
     * @param array/mixed $requestData.
     * @access public.
     * @return bool.
     */
    protected function isValidInput($requestData)
    {
        $isOk = true;

        if(!isset($requestData['username']) || !isset($requestData['password_hash']) || !isset($requestData['confirm_hash']))
        {
            $isOk = false;
        }

        return $isOk;
    }
}
?>