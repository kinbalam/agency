<?php
App::uses('FormAuthenticate', 'Controller/Component/Auth');

class ExtendedFormAuthenticate extends FormAuthenticate
{
    protected function _findUser($username, $password = null)
    {
        $cakephpVersion = Configure::version();
        $userModel = $this->settings['userModel'];
        list(, $model) = pluginSplit($userModel);
        $fields = $this->settings['fields'];

        $conditions = array('OR' => array(
            $model . '.' . $fields['username'] => $username,
            $model . '.' . 'alias' => $username)
        );

        if (!empty($this->settings['scope'])) {
            $conditions = array_merge($conditions, $this->settings['scope']);
        }

        $result = ClassRegistry::init($userModel)->find('first', array(
            'conditions' => $conditions,
            'recursive' => $this->settings['recursive'],
            'contain' => $this->settings['contain'],
        ));
        if (empty($result[$model])) {
            if(strpos($cakephpVersion, '2.4'))
            {
                $this->passwordHasher()->hash($password);
            }
            return false;
        }

        $user = $result[$model];
        if ($password) {

            if(strpos($cakephpVersion, '2.4'))
            {
                if (!$this->passwordHasher()->check($password, $user[$fields['password']])) {
                    return false;
                }
            }else
            {
                if($this->_password($password) != $user[$fields['password']])
                {
                    return false;
                }
            }
            unset($user[$fields['password']]);
        }

        unset($result[$model]);
        return array_merge($user, $result);
    }

    /**
    * Hash the plain text password so that it matches the hashed/encrypted password
    * in the datasource.
    *
    * @param string $password The plain text password.
    * @return string The hashed form of the password.
    */
    protected function _password($password)
    {
        return Security::hash($password, null, true);
    }
}
?>