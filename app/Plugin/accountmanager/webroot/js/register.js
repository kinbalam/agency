(function($){
    $(document).ready(function(){
        $('#send').on('click',function(){
            
            var $form        = $(this).parents('form');
            var $pswd        = $form.find('#password');
            var $confirm     = $form.find('#confirm');
            var $pswdHash    = $form.find('#pswd-hash');
            var $pswdConfirm = $form.find('#confirm-hash');
            
            $pswdHash.val(SHA1($pswd.val()));
            $pswd.val('');
            
            $pswdConfirm.val(SHA1($confirm.val()));
            $confirm.val('');
            
            $form.submit();
        });
    });
})(jQuery);