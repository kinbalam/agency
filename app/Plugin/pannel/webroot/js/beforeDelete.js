var beforeDeleteScreen = (function($, page){

    var ajaxRequests = 0;

    function disableControls() {
        $('button').hide();
    };

    function deleteRecords() {
        disableControls();
        $('#delete-msg').removeClass('hidden');
        $('.item-id').each(function(){
            ajaxRequests++;
            $.post('../admin_delete/' + $(this).val(),
                   function(data){
                        ajaxRequests--;
                        if (ajaxRequests == 0) {
                            $('#delete-msg').html(page.getText('panel', 'LBL_SUCESSFULL'));
                        }
                   });
        });
    };
    return{
        deleteRecords : deleteRecords
    };
})(jQuery, window);