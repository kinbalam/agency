$(document).ready(function() {



    function calc(){

        //Se recorta el precio de lista a dos decimales
        price =  parseFloat($('#ContractPrice').val());
        price = price.toFixed(2);
        $('#ContractPrice').val(price);



        //Si porcentaje es enable entonces lo que se calcula es el enganche
        if ($('#ContractPercentage').attr("readonly") == undefined){
            down_payment =  price *  $('#ContractPercentage').val()/100;
            porcentaje = parseFloat($('#ContractPercentage').val());
        }
        //Si enganche es enable se calcula es porcentaje
        else{
            porcentaje =  $('#ContractDownPayment').val()  * 100 / price;
            down_payment =  parseFloat($('#ContractDownPayment').val());
        }

        //Fijo a dos decimales
        porcentaje = porcentaje.toFixed(2);
        $('#ContractPercentage').val(porcentaje);

        //Fijo a dos decimales
        down_payment = down_payment.toFixed(2);
        $('#ContractDownPayment').val(down_payment);


        //Calculo de saldo 
        saldo = $('#ContractPrice').val()  -  down_payment;
        saldo = saldo.toFixed(2);
        $('#ContractBalance').val(saldo);

        $('.table input[type=text].span3').val($("#ContractPremonths").val());
        
        intereses = $('#ContractInterest').val();
        if (isFinite(porcentaje) && porcentaje <=100  && intereses >= 0 && saldo>=0){
            //muestra la tabla
            $('.table').show();
            //calcular letra adelantada y mensualidad normal
            $('.table input[type=text].span3').each(function(){
                meses = $(this).val();
                cell = $(this).parent().parent().parent();
                if (meses>0){
                    //calcula interes mensual
                    interes_mensual = saldo * intereses/100;

                    cell.find('input[type=text].span8').each(function(i){
                        if (i == 0){
                            mensualidad = saldo/meses;
                            $(this).val(mensualidad.toFixed(2));
                        }else if (i == 1){
                            mensualidad = saldo/meses + interes_mensual;
                            $(this).val(mensualidad.toFixed(2));
                        }else{
                            mensualidad = saldo/meses + interes_mensual +  interes_mensual * $('#QuoteTax').val() / 100;
                            $(this).val(mensualidad.toFixed(2));
                        }
                        $('#'+$(this).data('hidden')).val(mensualidad.toFixed(2));
                    });
                }else{
                    //limpia las celdas
                    cell.find('input[type=text].span8').each(function(i){
                            $(this).val('');
                            $('#'+$(this).data('hidden')).val('');
                    });
                }
            });
        }else{
            $('form .table').hide();
        }
        
    }

    $("input[type='text']").keypress(function(event) {
        if ( event.which == 13 ) {
            event.preventDefault();
            calc();
        }
    });

    $("input[type='text']").focusout(function() {
        calc();
    });

    $("input[type='radio']").click(function() {

        option = $(this).attr('value');
        console.log(option);
        if (option == 'percentage'){
            $('#ContractDownPayment').attr("readonly",true);
            $('#ContractPercentage').attr("readonly",false);
        }else{
            $('#ContractDownPayment').attr("readonly",false);
            $('#ContractPercentage').attr("readonly",true);
        }

    });

    $('#ContractOpPercentage').click();



});
