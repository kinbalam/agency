var usersAddScreen = (function($){

    function registerEvents() {
        $('.parent-permission').each(function(){
            $(this).on('click', function(){
                var checked = this.checked;
                $(this).next().children('input[type=checkbox]').each(function(){
                    this.checked = checked;
                });
            });
        });
    }

    return {
        registerEvents : registerEvents
    };
})(jQuery);

jQuery(function(){ //DOM ready.
    usersAddScreen.registerEvents();
});