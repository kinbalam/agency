var asoreToolbar = (function($)
{
    var settings = {module :'results'};
    var _container = '';
    var _controls  = [];

    function initialize(module)
    {
        settings.module = module;
        container = jQuery('.asoreToolbar');
        if (container) {
            _controls = container.find('.btn');
        }
    };

    function registerEvents()
    {
        _controls.each(function()
        {
            $(this).on('click',function(){
                
                if ($(this).hasClass('edit-action'))
                {
                    var recordSelectedValue = $('#recordSelected').val();
                    
                    if (recordSelectedValue != '') {
                        document.location.href = 'admin_edit/' + recordSelectedValue;
                    }
                }
                
                if ($(this).hasClass('delete-action'))
                {
                    var recordSelectedValue = $('#recordSelected').val();
                    if (recordSelectedValue) {
                        document.location.href = 'beforeDelete/' + recordSelectedValue;
                    }
                }
                
                if ($(this).hasClass('add-action'))
                {
                    document.location.href = 'admin_add';
                }
                
                if ($(this).hasClass('save-action')) {
                    $('#form-data').submit();
                }
                
                if($(this).hasClass('grid-view'))
                {
                    $('#view-type').val('grid');
                    $('#form-view').submit();
                }

                if($(this).hasClass('table-view'))
                {
                    $('#view-type').val('table');
                    $('#form-view').submit();
                }
             });
        });
    };

    return{
        init : initialize,
        registerEvents : registerEvents
    };
})(jQuery);