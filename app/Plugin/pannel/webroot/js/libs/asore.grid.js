var asoreGrid = (function($){

    var settings = {type : '', ele : '.asore-grid'};
    var selectedItems = [];

    function initialize(){
        registerControlsBehavior();
    };

    function registerControlsBehavior(){
        $(settings.ele).find('input[type=checkbox]').each(function(item){
            $(this).on('click', function(){
                var value, indexOf;

                value = $(this).attr('id');
                indexOf = selectedItems.indexOf(value);

                if (this.checked) {
                    if (indexOf < 0) {
                        selectedItems.push($(this).attr('id'));
                    }
                }else
                {
                    if (indexOf >= 0) {
                        selectedItems[indexOf] = undefined;
                    }
                }

                $('#recordSelected').val(generateSelectedValueCSV(selectedItems));
            });
        });
    };

    function generateSelectedValueCSV(items) {
        var selectedValuesCSV = '';
        for (var index = 0; index < items.length; index++) {
            if (items[index]) {
                selectedValuesCSV += items[index] + ',';
            }
        }

        if (selectedValuesCSV.length > 0) {
            // removing last trailing ','.
            selectedValuesCSV = selectedValuesCSV.substring(0, selectedValuesCSV.length - 1);
        }
        return selectedValuesCSV;
    }

    return {
        init : initialize,
        registerEvents : registerControlsBehavior
    };
})(jQuery);