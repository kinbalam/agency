(function (window, myApp) {
    window.loadNextRecords = function(){
        jQuery.post('loadNextRecords', 
                    jQuery('#form-view').serialize(),
                    function(data)
                    {
                        jQuery('#records-container').append(data).trigger('dom:updated');
                    }
                   );
    };

    window.getText = function(domain, label){
        var domain = myApp.general.labels[domain] || {};
        var text = domain[label] || label;
        return text;
    };
})(window, myApp);