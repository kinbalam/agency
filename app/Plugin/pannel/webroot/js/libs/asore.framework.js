var __globalMethods = 
{
    createNamespace: function(parentNS,namespace)
    {
        var namespaces       = namespace.split('.');
        var namespacesLength = namespaces.length;
    
        for(var index=0;index<namespacesLength;index++)
        {
            if(!(namespaces[index] in parentNS))
            {
                parentNS[namespaces[index]] = {};
            }
            parentNS =  parentNS[namespaces[index]];
        }
    },
    createProduct: function()
    {
        this.myApp = {};
        this.createNamespace(this.myApp, "general");
        this.myApp.general.url        = '';
        this.myApp.general.systemName = 'test product';
        this.myApp.general.controller = '';
        return this.myApp;
    }
};

var asore = {};
jQuery.extend(asore, __globalMethods);
