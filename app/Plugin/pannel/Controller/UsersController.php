<?php
class UsersController extends PannelAppController
{
    public $name = 'Users';
    public $components = array('AccountManager.AccountServices');

    /**
     *
     * beforeFilter.
     *
     *
     *
     *
     */
    public function beforeFilter()
    {
        $this->ignoreAction(array('logout'));
        $this->mapAction('read',array('admin_index'));

        parent::beforeFilter();
    }

    public function index(){
    }

    /**
     *
     * admin_profile
     *
     * @param void.
     *
     * @access public.
     * @return void.
     *
     */
    public function admin_profile()
    {
        //$this->setActiveMenu('Users');
    }

    /**
     *
     * admin_add.
     *
     * 
     * @param void.
     * @access public.
     * @return void.
     */
    public function admin_add()
    {
        $isOk = true;

        if($this->request->is('post'))
        {
            $active = 0;
            if(isset($this->request->data['User']['active']))
            {
                $active = 1;
            }

            if(!isset($this->request->data['User']['aro_id']) || empty($this->request->data['User']['aro_id']))
            {
                $isOk = false;
                $this->showErrorMessage(__d('pannel', 'ERR_ROLE_NOT_DEFINED'));
            }else
            {
                $data['username'] = $this->request->data['User']['mail'];
                $data['password_hash'] = sha1($this->request->data['User']['password']);
                $data['confirm_hash'] = sha1($this->request->data['User']['confirm']);
                $data['aro_id'] = $this->request->data['User']['aro_id'];
                $data['alias'] = $this->request->data['User']['username'];
            
                $this->loadModelAs('Pannel.UserAccount','Account');
                $this->Account->Behaviors->load('AccountManager.UsersManager');
                $isOk = $this->AccountServices->saveNewUser($data, $active);

                if(!$isOk)
                {
                    $this->showErrorMessage($this->AccountServices->errorMessage);
                }else
                {
                    $this->showInfoMessage(__d('pannel', 'LBL_SAVED_RECORD'));
                }
            }
        }
        
        $this->PannelAcl->Aro->unbindModel(array('hasAndBelongsToMany'=>array('Aco')));
        $this->PannelAcl->Aro->displayField = 'alias';
        $aros = $this->PannelAcl->Aro->find('list',
                                            array('fields'=>
                                                  array('id','alias'),
                                                  'conditions'=>
                                                  array('parent_id'=>NULL)));
            
        $acos = $this->PannelAcl->Aco->find('list', array('fields'=>'id, alias'));
        $this->set('groups',$aros);
        $this->set('permissions', $acos);
        
    }

    /**
     *
     *
     * admin_changeUserName.
     *
     * @param void.
     *
     * @access public.
     * @return void.
     */
    public function admin_changeUserName()
    {
        $this->loadModel('AccountManager.User');
        
        if(!empty($this->request->data))
        {
            
            App::uses('AclComponent','Controller/Component');
            $component = new AclComponent(new ComponentCollection());
            $this->Acl = $component;
            
            $aro        = $this->Acl->Aro;
            $user       = $this->Session->read('Auth.User');
            
            $data['User']['description'] = $this->request->data['User']['name'];
            $data['User']['id']          = $user['id'];
            
            
            $this->User->set($data);
            if($this->User->validates())
            {
                $this->User->begin();
                
                $aroData['Aro']['id']    = $user['aro_id'];
                $aroData['Aro']['alias'] = $this->request->data['User']['name'];
                
                $aro->set($aroData);
                if($aro->save() && $this->User->save())
                {
                    $this->User->commit();
                    $this->showAlert('Cambios realizados','success','Enhorabuena!');
                    
                }else
                {
                    $this->User->rollback();
                    $this->showAlert('Un problema acaba de ocurrir.','error');
                }
            }else
            {
                $field   = key($this->User->validationErrors);
                $message = current($this->User->validationErrors[$field]);
                $this->showAlert($message,'error','Verifica tus datos!');
            }
            
            
        }else
        {
        }
    }

    /**
     *
     * admin_changePassword.
     *
     * @param void.
     * 
     * @acccess public.
     * @return void.
     */
    public function admin_changePassword()
    {
        $this->loadModel('AccountManager.User');
    }

    /**
     *
     * logout.
     *
     *
     * @param void.
     *
     * @access public.
     * @return void.
     */
    public function logout()
    {
        $this->Session->delete('Acl.Permissions');
        $this->redirect('/account_manager/accounts/logout');
    }

    /**
     *
     * admin_index.
     * 
     * @param void.
     * @access public.
     * @return void.
     */
    public function admin_index()
    {
        $this->_config['view']['panel']['isAjax']  = true;
    }

    /**
     *
     * admin_delete.
     *
     * Delete a record.
     *
     * @param int $id.
     * @access public.
     * @return mixed.
     */
    function admin_delete($id = 0)
    {
        $this->layout = '';
        $json = array('code'=>0, 'responseText'=> '');
        $this->loadModelAs('Pannel.UserAccount','Account');
        $this->Account->id = $id;
        $data = $this->Account->read();
        if($this->Account->delete($id))
        {
            $json['code'] = 200;
            $json['responseText'] = 'OK';
        }else
        {
            $json['code'] = 500;
            $json['responseText'] = __d('pannel', 'ERR_DELETING RECORD');
        }

        $this->set('response', $json);
        $this->render('/Common/json');
    }

    public function loadPermissions($aroId = 0)
    {
        $permissions = $this->PannelAcl->Aro->Permission->find('all', array('conditions' => array('aro_id' => $aroId)));
    }
    /**
     *
     * getDataColumns.
     *
     * 
     *
     */
    protected function getDataColumns()
    {
        return array('id','description','active');
    }
}
?>
