<?php
class VehiclesController extends PannelAppController
{
    public $name = 'Vehicles';
    public $components = array('AccountManager.AccountServices');
    public $paginate = array(
        'Vehicle'=>array(
            'limit' => 10,
            'order' => array(
                'Vehicle.id' => 'desc'
            ),
            'conditions' => array('active'=>1)
        )
    );
    
    /**
     *
     * beforeFilter.
     *
     *
     *
     *
     */
    public function beforeFilter()
    {
        $this->mapAction('read',array('index','view'));

        parent::beforeFilter();
    }


    /**
     * The vehicles list
     */
    public function index(){

        $this->Paginator->settings = $this->paginate;

        if ($this->request->is('post')){
            $q = $this->request->data['Vehicle']['q'];
            $conditions = array();
            $conditions['active'] = 1;
            $conditions['OR']['Vehicle.brand like '] = "%$q%";
            $conditions['OR']['Vehicle.model like '] = "%$q%";
            $conditions['OR']['Vehicle.year like '] = "%$q%";
            $conditions['OR']['Vehicle.id like '] = "%$q%";

            try {
                $data = $this->Paginator->paginate('Vehicle',$conditions);
            } catch (NotFoundException $e) {
                //Do something here like redirecting to first or last page.
                //$this->request->params['paging'] will give you required info.
                $this->request->params['named']['page'] = 1;
                $data = $this->Paginator->paginate('Vehicle',$conditions);

            }
        }else{
            $data = $this->Paginator->paginate('Vehicle');
        }

        $this->set('data', $data);
    }

    /**
     * Add a new vehicle
     */
    public function add(){

        if ($this->request->is('post')){
            // If the form data can be validated and saved...
            if ($this->Vehicle->save($this->request->data)) {
                // Set a session flash message and redirect.
                $this->Session->setFlash('Se agrego un vehículo nuevo', 'default', array(), 'good');
                return $this->redirect('/pannel/vehicles/');
            }
        }

    }

    /**
     * Show vehicle's information
     * @param $id
     */
    public function view($id){
        $data = $this->Vehicle->findById($id);
        $this->set('data',$data['Vehicle']);
    }

    public function delete($id){
        if ($this->request->is('delete')){
            $this->Vehicle->read(null, $id);
            $this->Vehicle->set('active', 0);
            if ($this->Vehicle->save())
                $this->Session->setFlash('Se elimino correctamente el folio '.$id, 'default', array(), 'good');
            else
                $this->Session->setFlash('No se pudo eliminar el folio '.$id, 'default', array(), 'bad');
            return $this->redirect('/pannel/vehicles/index');

        }else{
            $this->request->data =   $this->Vehicle->findById($id);;
            $this->render('/vehicles/delete');
        }
    }

    public function loadPermissions($aroId = 0)
    {
        $permissions = $this->PannelAcl->Aro->Permission->find('all', array('conditions' => array('aro_id' => $aroId)));
    }

    /**
     *
     * getDataColumns.
     *
     * 
     *
     */
    protected function getDataColumns()
    {
        return array('id','description','active');
    }
}
?>
