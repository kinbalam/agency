<?php
App::uses('AclComponent','Controller/Component');
class PannelAclComponent extends AclComponent
{
    
    public $components = array('Session');
    public static $sessionKey = 'Acl.Permissions';
    
    /**
     *
     * loadPermissions.
     *
     * @param array/mixed $aroRecord.
     * @param bool $reload.
     * 
     * @access public.
     * @return void.
     */
    public function loadPermissions($aroRecord,$reload=false)
    {
        if( !($this->Session->check(self::$sessionKey)) || $reload )
        { 
            $permissions    = $this->_Instance->getPermissions($aroRecord);
            $this->Session->write(self::$sessionKey,$permissions);
        }
    }
    
    /**
     *
     * setUserAroData.
     *
     * 
     * @access public.
     * @return array/mixed.
     */
    public function setUserAroData($user,$aroRecord)
    {
        $user['aro_parent_id'] = $aroRecord['Aro']['parent_id'];
        $user['aro_parent_name']  = $this->_Instance->getAroDescription($aroRecord['Aro']['parent_id']);
        $user['aro_id'] = $aroRecord['Aro']['id'];
        return $user;
    }
    
    /**
     *
     * getAroRecord.
     *
     * 
     * @param int $userId current user id.
     * @access public.
     * @return array/mixed.
     */
    public function getAroRecord($userId)
    {
        return $this->_Instance->getAroRecord($userId);
    }
    
    /**
     *
     * getUserAroData
     *
     * @param array/mixed $user.
     * @access public.
     * @return array/mixed.
     */
    public static function getUserAroData($user)
    {
        if(isset($user['aro_id']))
        {
            return array('Aro'=>
                         array('parent_id'=>$user['aro_parent_id'],
                               'aro_id'=>$user['aro_id'])
                        );
        }
        
        return array();
    }
    
    /**
     *
     * getPermissions.
     *
     * @param void.
     * @access public static.
     * @return array/mixed.
     */
    public static function getPermissions()
    {
        return CakeSession::read(self::$sessionKey);
    }
    
    /**
     *
     * isAutorized.
     *
     * Determines if the passed controller/action is authorized.
     *
     * @param string $controller.
     * @param string $action.
     * @access public.
     * @return bool.
     */
    public static function isAuthorized($controller,$action)
    {
        $isAuthorized = false;
        $controller   = strtolower($controller);
        $permissions  = CakeSession::read(self::$sessionKey);
        
        if(isset($permissions[$controller]) && isset($permissions[$controller][$action]))
        {
            $isAuthorized = ($permissions[$controller][$action]>0)? true:false;
        }
        
        return $isAuthorized;
    }
}
?>