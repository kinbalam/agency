<?php
App::uses('DbAcl','Controller/Component/Acl');
class PannelDbAcl extends DbAcl
{
    /**
     *
     * getAroRecord.
     *
     * 
     * @param int $userId current user id.
     * @access public.
     * @return array/mixed.
     */
    public function getAroRecord($userId)
    {
        $aro = $this->Aro;
        $aroRecord = $aro->find('first',
                                array('conditions'=>
                                      array('foreign_key'=>$userId))
                                );
        return $aroRecord;
    }
    /**
     *
     * getPermissions.
     *
     * @params array/mixed user.
     * 
     * @access public.
     * @return void.
     */
    public function getPermissions($aroRecord)
    {
        $permissions      = null;
        $finalPermissions = null;
        
        if(!empty($aroRecord))
        {
            $this->Aco->unbindModel(array('hasAndBelongsToMany' => array('Aro')), false);
            $this->Aco->virtualFields = array('aliasLower' => 'LOWER(alias)');
            $acos = $this->Aco->find('all',array('fields' => array('aliasLower', '_ismenu')));
            
            foreach($acos as $aco)
            {
                $ismenu = 0;
                
                if(isset($aco['Aco']['_ismenu']))
                    $ismenu = $aco['Aco']['_ismenu'];
                    
                $permissions[$aco['Aco']['aliasLower']]['read']   = 0;
                $permissions[$aco['Aco']['aliasLower']]['create'] = 0;
                $permissions[$aco['Aco']['aliasLower']]['update'] = 0;
                $permissions[$aco['Aco']['aliasLower']]['delete'] = 0;
                $permissions[$aco['Aco']['aliasLower']]['ismenu'] = $ismenu;
            }
            
            $parentPermissions = $this->getAroPermissions($aroRecord['Aro']['parent_id'],$permissions);
            $ownPermissions    = $this->getAroPermissions($aroRecord['Aro']['id'],$permissions);
            
            foreach($permissions as $controller=>$permission)
                foreach($permission as $action=>$value)
                {
                    if($action!='ismenu')
                    {
                        $groupFlag =  $parentPermissions[$controller][$action];
                        $ownFlag   =  $ownPermissions[$controller][$action];
                        $finalPermissions[$controller][$action] = $groupFlag + $ownFlag;
                    }else
                    {
                        $finalPermissions[$controller]['ismenu'] = $permissions[$controller]['ismenu'];
                    }
                }
        }
        
        return $finalPermissions;
    }
    
    /**
     *
     * getAroPermissions.
     *
     * 
     * @param int          $aroId.
     * @param array/mixed  $permissions.
     *
     * @access private.
     * @return array/mixed.
     */
    public function getAroPermissions($aroId, $permissions)
    {
        $globalPermissions = array();
        $fields = '_read AS read,_update AS update,_create AS create,_delete AS delete,Aco.alias';
        $dbPermissions  = $this->Aro->Permission->find('all',array('conditions'=>array('aro_id'=>$aroId),
                                                                        'fields'=>$fields));
        
        if(!empty($dbPermissions))
        {
            foreach($permissions as $alias => $permission)
            {   
                $controllerPermission = array();
                
                foreach($dbPermissions as $dbPermission)
                {
                    if(isset($dbPermission['Permission']))
                    {
                        if(strtolower($dbPermission['Aco']['alias'])=='controllers' && $alias=='controllers')
                        {
                            $globalPermissions = $dbPermission;
                            break;
                        }
                        
                        if(strtolower($dbPermission['Aco']['alias'])==$alias)
                        {
                            $controllerPermission = $dbPermission;
                            break;
                        }
                    }
                }
                
                if(empty($controllerPermission))
                {
                    $controllerPermission = $globalPermissions;
                }else
                {
                    $controllerPermission['Permission']['create'] += $globalPermissions['Permission']['create'];
                    $controllerPermission['Permission']['update'] += $globalPermissions['Permission']['update'];
                    $controllerPermission['Permission']['delete'] += $globalPermissions['Permission']['delete'];
                    $controllerPermission['Permission']['read']   += $globalPermissions['Permission']['read'];
                }
                
                if(!empty($controllerPermission))
                   $permissions[$alias] = $controllerPermission['Permission'];
            }
        }
        
        return $permissions;
    }
    
    /**
     *
     * getAroDescription.
     *
     * 
     * @param int $aroId.
     * 
     * @access private.
     * @return string
     */
    public function getAroDescription($aroId)
    {
        $aro = $this->Aro;
        $aro->unbindModel(
                          array('hasAndBelongsToMany'=>array('Aco'))
                         );
        
        $record = $aro->find('first',array('conditions'=>array('id'=>$aroId)));
        if(!empty($record))
            return $record['Aro']['alias'];
            
        return '';
    }
}
?>