<?php
class ConfigurationsController extends PannelAppController
{
    public $name = 'Configurations';
    public $uses = array();
    public $components = array('AccountManager.AccountServices');
    
    public function beforeFilter()
    {
        $this->allowAction('index');
        parent::beforeFilter();
        $this->layout = 'configuration';
        Configure::write('Config.language','eng');
    }
    
    /**
     *
     * index.
     *
     * Configuration entry point.
     *
     * @acces public.
     * @return void.
     */
    public function index()
    {
        $isOk = false;
        $msg  = '';
        
        if($this->request->is('Post'))
        {
            $this->loadModelAs('Pannel.UserAccount','Account');
            
            $data['username']      = 'system';
            $data['password_hash'] = sha1($this->request->data['Configuration']['password']);
            $data['confirm_hash']  = sha1($this->request->data['Configuration']['confirm']);
            
            $user = $this->Account->find('first',array('conditions'=>array('description'=>'system')));
            if(empty($user))
            {
                $data['aro_id']        = 1;
                $data['alias']         = 'system';
                
            }else
            {
                $data['id']            = $user['UserAccount']['id'];
            }
            
            $this->Account->Behaviors->load('AccountManager.UsersManager');
            if($this->AccountServices->saveNewUser($data,1))
            {
                $this->render('complete');
            }else
            {
                $msg = $this->AccountServices->errorMessage;
            }
        }
        
        $this->set('message',$msg);
    }
}
?>