<?php

App::uses('CakeEmail', 'Network/Email');

class ContractsController extends PannelAppController
{
    public $name = 'Contracts';
    public $components = array('AccountManager.AccountServices');
    public $uses =  array('Pannel.Vehicle', 'Pannel.Contract' );
    public $paginate = array(
        'Contract'=>array(
            'limit' => 10,
            'order' => array(
                'Contract.id' => 'desc'
            ),
            'conditions' => array('Contract.active'=>1)
        )
    );
    /**
     *
     * beforeFilter.
     *
     *
     *
     *
     */
    public function beforeFilter()
    {
        $this->mapAction('read',array('index','view','pdf','send_email'));
        parent::beforeFilter();
    }


    /**
     * The vehicles list
     */
    public function index(){

        $data = $this->Vehicle->find('all');

        //debug($data);

        $this->Paginator->settings = $this->paginate;

        if ($this->request->is('post')){
            $q = $this->request->data['Contract']['q'];
            $conditions = array();
            $conditions['Contract.active'] = 1;
            $conditions['OR']['Vehicle.brand like '] = "%$q%";
            $conditions['OR']['Vehicle.model like '] = "%$q%";
            $conditions['OR']['Contract.client_name like '] = "%$q%";
            $conditions['OR']['Contract.endorsement_name like '] = "%$q%";

            try {
                $data = $this->Paginator->paginate('Contract',$conditions);
            } catch (NotFoundException $e) {
                //Do something here like redirecting to first or last page.
                //$this->request->params['paging'] will give you required info.
                $this->request->params['named']['page'] = 1;
                $data = $this->Paginator->paginate('Contract',$conditions);

            }
        }else{
            $data = $this->Paginator->paginate('Contract');
        }

        $this->set('data', $data);
    }

    /**
     * Add a new quote
     */
    public function add(){


        if ($this->request->is('post')){
            foreach ($this->request->data['QuoteDet'] as $key => $value) {
                if (empty($value['months']))
                    unset($this->request->data['QuoteDet'][$key]);
            }
            if($this->Quote->saveAssociated($this->request->data)){
                $quote = $this->Quote->findById($this->Quote->id);
                $this->mail($quote['Quote']['email'],$quote);

                $this->redirect(array('action'=>'view',$this->Quote->id));
            }
        }else{
            
            $this->Vehicle->virtualFields['info'] = 'CONCAT(Vehicle.brand, " ", Vehicle.model,". ",Vehicle.year,". ",Vehicle.color)';

            $vehicles = $this->Vehicle->find('list',
            array(
                    'conditions'=>array('active'=>1),
                    'fields'=>array('info')
                )
            );
            $this->set('vehicles',$vehicles);
        }
        
    }


    /**
     * Show vehicle's information
     * @param $id
     */
    public function view($id){
        $data = $this->Quote->findById($id);
        $this->set('quote',$data);
    }

    /**
     * Show view in PDF  
     */
    public function pdf($id){
        App::import('Vendor', 'Fpdf', array('file' => 'fpdf/fpdf.php'));

        $quote = $this->Quote->findById($id);
        $this->set('quote',$quote);

        $this->layout = '';
        $this->response->charset('UTF-8');

        $this->response->type('pdf');
        //$this->response->send();

        $this->set('pdf', new FPDF('P','mm','Letter'));
    }

    public function send_email($id){

        $quote = $this->Quote->findById($id);

        if (isset($this->request->data['email'])){
            $this->mail($this->request->data['email'],$quote);
        }
        $this->set('quote',$quote);
        $this->render('view');

        //http://windows.microsoft.com/es-MX/windows/outlook/send-receive-from-app
    }

    public function delete($id){
        if ($this->request->is('delete')){
            if ($this->Quote->delete($id))
                $this->Session->setFlash('Se elimino correctamente el folio '.$id, 'default', array(), 'good');
            else
                $this->Session->setFlash('No se pudo eliminar el folio '.$id, 'default', array(), 'bad');
            return $this->redirect('/pannel/quotes/index');

        }else{
            $this->request->data =   $this->Quote->findById($id);;
            $this->render('/quotes/delete');
        }
    }

    public function mail($email,$quote){
        App::import('Vendor', 'Fpdf', array('file' => 'fpdf/fpdf.php'));
        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(40,10,'¡Hola, Mundo!');
        $data = $pdf->Output('cotizacion.pdf','S');
        //echo $data;

        $Email = new CakeEmail('asore');
        $Email->template('quote')
            ->emailFormat('html')
            ->from(array('autosduran@asore.mx' => 'Autos Durán'))
            ->to($email)
            ->subject('Cotización Autos Durán')
            ->viewVars(array('quote'=>$quote))
            //->attachments(array('attachement1.pdf' => array('data' => chunk_split(base64_encode($data)))))
            ->send();
    }
}

