<?php
App::uses('PannelDbAcl','Pannel.Controller/Component/Acl');
App::uses('PannelAclComponent','Pannel.Controller/Component/Acl');

class PannelAppController extends AppController
{
    protected $_name = 'Panel';
    protected $_config = array('view'=>
                              array('nav'=>
                                    array('show'=>true),
                                    'menu'=>
                                    array('show'=>true,
                                          'active'=>'home'
                                         ),
                                    'breadcrumb'=>array(),
                                    'panel'=>array('type'=>'table',
                                                   'isAjax'=>false,
                                                   'columns'=>array(),
                                                   'mappedColumns'=>array()
                                                  ),
                                    'alert' => array('show' => false,
                                                     'message' => 'test message',
                                                     'title' => 'test title',
                                                     'type' => 'info'),
                                   ),
                              'permissions'=>
                              array('active'=>true,
                                    'mappedActions'=>array('read'=>array('index', 'index'),
                                                           'create'=>array('add', 'admin_add'),
                                                           'delete' => array('delete','admin_delete', 'beforeDelete')),
                                    'ignoredActions'=>array()
                                   )
                            );
    protected $_user;
    public $dataSet;
    
    public $helpers = array('Pannel.UserEntity','Pannel.Menu');
    public $components = array('Pannel.PannelAcl','AccountManager.ExtendedAuth','Session','Paginator');


    /**
     *
     * beforeFilter.
     *
     * @access public.
     * @return mixed/html.
     */
    public function beforeFilter()
    {
        $this->ExtendedAuth->setDefaultConfiguration();
        $this->_user  = $this->ExtendedAuth->user();
        
        if($this->_config['permissions']['active'] && $this->_user)
        {
            $this->mapAction('read', array('loadNextRecords', 'loadLayout'));
            
            if(!PannelAclComponent::getUserAroData($this->_user))
            {
                $this->setupAppPermissions();
            }
            
            if(!$this->isUserAuthorized())
            {
                throw new MethodNotAllowedException('You are no authorized to access this URI.');
            }
        }
        
        $this->layout = 'panel';
        $this->setActiveMenu($this->name);
        
    }
    
    /**
     *
     * setupAppPemissions.
     * 
     *
     * @param void.
     * @access public.
     * @return void.
     */
    public function setupAppPermissions()
    {
        $aroRecord = $this->PannelAcl->getAroRecord($this->_user['id']);
        $this->PannelAcl->loadPermissions($aroRecord);
        $userUpdated = $this->PannelAcl->setUserAroData($this->_user,$aroRecord);
        ExtendedAuthComponent::updateUser($userUpdated);
    }
    
    /**
     *
     * isUserAuthorized.
     *
     * @param void.
     * @access public.
     * @return bool.
     */
    private function isUserAuthorized()
    {
        $actionPermission = false;
        $action           = $this->request->params['action'];
        $controller       = $this->request->params['controller'];
        
        $isIgnoredAction = in_array($action,$this->_config['permissions']['ignoredActions']);
            
        if(!$isIgnoredAction)
        {
            if(!in_array($action,array('read','update','delete')))
            {
                $action = $this->getMappedAction($action);
            }
                
            $actionPermission = PannelAclComponent::isAuthorized($controller,$action);
        }else
        {
            $actionPermission = true;
        }
        
        return $actionPermission;
    }
    
    /**
     *
     * getMappedAction.
     *
     * @param string $currentAction.
     * @access public.
     * @return string.
     */
    private function getMappedAction($currentAction)
    {
        $mappedAction = $currentAction;
        
        foreach($this->_config['permissions']['mappedActions'] as $action=>$mappedActions)
        {
            if(in_array($currentAction,$mappedActions))
            {
                $mappedAction = $action;
                break;
            }
        }
        
        return $mappedAction;
    }
    
    /**
     *
     * mapAction.
     *
     * @param string $crudAction.
     * @param string $action.
     * @access protected.
     * @return void.
     */
    protected function mapAction($crudAction, $action)
    {
        $actions = $action;

        if(!is_array($actions))
        {
            $actions = array($actions);
        }

        $mappedActions = $this->_config['permissions']['mappedActions'][$crudAction];
        $mappedActions = array_merge($mappedActions,$actions);
        $this->_config['permissions']['mappedActions'][$crudAction] = $mappedActions;
    }
    
    /**
     *
     * ignoreAction.
     *
     * @param string $action
     * @access protected.
     * @return void.
     */
    protected function ignoreAction($action)
    {
        $actions = $action;
        if(!is_array($actions))
        {
            $actions = array($actions);
        }
        
        $ignoredActions = $this->_config['permissions']['ignoredActions'];
        $ignoredActions = array_merge($ignoredActions,$actions);
        
        $this->_config['permissions']['ignoredActions'] = $ignoredActions;
    }
    
    /**
     *
     * setActiveMenu.
     *
     * Sets the current menu key in the $_config.
     * 
     * @param string $menuKey.
     * @access protected.
     * @return void.
     */
    protected function setActiveMenu($menuKey)
    {
        $this->_config['view']['menu']['active'] = strtolower($menuKey);
    }
    
    /**
     *
     * allowAction.
     *
     * 
     *
     * @param string/array $actions.
     * @access protected.
     * @return void.
     */
    protected function allowAction($actions)
    {
        if(empty($actions))
        {
            return;
        }
        
        if(!is_array($actions))
        {
            $actions = array($actions);
        }
        
        $this->ExtendedAuth->allowedActions = array_merge($this->ExtendedAuth->allowedActions,$actions);
        $this->ignoreAction($actions);
    }
    
    /**
     *
     * beforeRender.
     *
     * @access public.
     * @return void.
     */
    public function beforeRender()
    {
        if($this->request->is('Post') && isset($this->request->data['viewConfig']))
        {
            $this->_config['view']['panel']['type'] = $this->request->data['viewConfig']['viewType'];
        }
        
        $this->_config['view']['panel']['mappedColumns'] = $this->getMappedGridDataColumns();
        $this->_config['view']['panel']['columns'] = $this->getDataColumns();
        
        $this->set('viewData',$this->_config['view']);
        $this->set('module',$this->name);
    }

    /**
     *
     * beforeDelete.
     *
     *
     *
     */
    public function beforeDelete($ids = '')
    {
        $multiple = false;
        $conditions = array();
        $model   = Inflector::singularize($this->name);

        if(strpos($ids, ',') > 0)
        {
            $multiple = true;
        }

        if($multiple)
        {
            $ids = explode(',', $ids);
            foreach($ids as $id)
            {
                $conditions[] = array('id' => $id);
            }
            $conditions = array('OR' => $conditions);
        }else
        {
            $conditions = array('id' => $ids);
        }

        $records = $this->{$model}->find('all', array('conditions' => $conditions));
        $this->set('recordsToDelete', $records);
        $this->render('Common/before_delete');
    }
    /**
     *
     * getDataColumns.
     *
     * @ access public.
     * @ return array/mixed.
     */
    protected function getDataColumns()
    {
        $columns = array();
        
        if(!empty($this->dataSet))
        {
            $firsRecord = current($this->dataSet);
            $columns = array_keys($firsRecord[Inflector::singularize($this->name)]);
        }
        
        return $columns;
    }
    
    /**
     *
     * getMappedGridDataColumns.
     *
     *
     * @access public.
     * @return array/mixed.
     */
    public function getMappedGridDataColumns()
    {
        return array('name'=>'description');
    }
    
    /**
     *
     * loadNextRecords.
     *
     * Ajax requests.
     * 
     * @access public.
     * @return mixed/html.
     */
    public function loadNextRecords()
    {
        if($this->request->isAjax())
        {
            $model   = Inflector::singularize($this->name);
            $columns = $this->getDataColumns();
            $this->layout = '';
            $users = $this->{$model}->find('all',array('fields'=>$columns));
            
            $this->_config['view']['panel']['type'] = $this->request->data['viewConfig']['viewType'];
            $this->set('records',$users);
            $this->render('Panel/load_next_records');
            
        }else
        {
            throw new BadRequestException('error');
        }
    }
    
    
    /**
     *
     * loadLayout.
     *
     *
     *
     */
    public function loadLayout()
    {
        if($this->request->isAjax())
        {
            $model   = Inflector::singularize($this->name);
            $columns = $this->getDataColumns();
            $this->layout = '';
            
            $this->_config['view']['panel']['isAjax'] = true;
            $this->_config['view']['panel']['type']   = $this->request->data['viewConfig']['viewType'];
            $this->render('Panel/load_layout');
            
        }else
        {
            throw new BadRequestException('error');
        }
    }
    
    /**
     *
     * loadModelAs.
     * 
     * Registers a model useing the name passed into $as. 
     *
     * @param string $modelName Model to load
     * @param string $as Name to use to load the model.
     * 
     * @return bool.
     */
    public function loadModelAs($modelName,$as='')
    {
        if(empty($as))
        {
            throw new CakeException(__d('pannel', 'MISSING_PARAM_%s','as'));
        }
        
        if(empty($modelName))
        {
            throw new CakeException(__d('pannel', 'MISSING_PARAM_%s','modelName'));
        }

        $this->{$as} = ClassRegistry::init($modelName);
        return true;
    }

    /**
     *
     * showAlertMessage.
     *
     * Shows system wide messages.
     *
     * @param string type. Type of message (info, error, warning)
     * @param string text. Message text.
     * @acces protected.
     * @return void.
     */
    protected function showAlertMessage($type, $text)
    {
        $this->_config['view']['alert']['show']    = true;
        $this->_config['view']['alert']['type']    = $type;
        $this->_config['view']['alert']['message'] = $text;
    }

    /**
     *
     * showInforMessage.
     *
     *
     */
    protected function showInfoMessage($message)
    {
        $this->showAlertMessage('info', $message);
    }

    /**
     *
     * showErrorMessage.
     *
     *
     *
     */
    protected function showErrorMessage($message)
    {
        $this->showAlertMessage('error', $message);
    }
}
?>