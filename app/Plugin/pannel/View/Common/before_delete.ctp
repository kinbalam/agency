<?php
echo $this->Html->script('/pannel/js/beforeDelete.js', false)
?>
<h3>Deseas eliminar los siguientes registros:</h3>
<ul>
<?php
foreach($recordsToDelete as $records)
{
?>
    <li>
    <?php
    foreach($records as $model => $columns)
    {
        echo $model . ' / id:' . $columns['id'];
    ?>
    <input class="item-id" type="hidden" value="<?php echo $columns['id'];?>" />
    <?php
    }
    ?>
    </li>
<?php
}
?>
</ul>
<hr />
<button class="btn btn-medium" onclick="beforeDeleteScreen.deleteRecords();"><?php echo __d('panel', 'LBL_CONTINUE');?></button>
<button class="btn btn-medium btn-danger" onclick="document.location.href = '../admin_index'"><?php echo __d('panel', 'LBL_CANCEL');?></button>
<h4 id="delete-msg" class="hidden"><?php echo __d('pannel', 'LBL_DELETING');?></h4>