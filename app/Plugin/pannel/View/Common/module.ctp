<?php
if($viewData['panel']['isAjax'])
{
?>
<script type="text/javascript">
    var isAjax = true;
</script>
<?php
}
echo $this->Html->script('/pannel/js/libs/asore.grid', false), 
     $this->Element('main_toolbar',
                    array('crumbs' => 
                          array('list' => $module,
                                'active' => $module)));
?>
<script type="text/javascript">
    jQuery(function(){
        jQuery('body').on('dom:updated', function(){
            asoreGrid.init();
        });

        if (isAjax) {
            window.loadNextRecords();
        }
    });
</script>
<div class="row-fluid">
    <div id="data-container" class="span9">
        <?php
        echo $this->fetch('content');
        ?>
    </div>
</div>