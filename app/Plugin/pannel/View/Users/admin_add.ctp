<?php
echo $this->Element('record_toolbar', array('viewAlias' => 'add')), 
     $this->Form->create('User', array('id' => 'form-data')),
     $this->Html->script('/pannel/js/users/add.js', false);
?>
<div class="row">
    <div class="span2">
        <a href="#">
        <?php
        echo $this->Html->image('cake.icon.png',array('width'=>120,'class'=>'img-polaroid'));
        ?>
        </a>
    </div>
    <div class="span7">
        <div class="row">
            <div class="span3">
                <label class="control-label" for="inputName">Username</label>
                <div class="controls">
                    <input type="text" name="data[User][username]" id="inputName" placeholder="Username" />
                </div>
            </div>
            <div class="span2">
                <label class="control-label" for="inputEmail">Email</label>
                <div class="controls">
                    <input type="text" name="data[User][mail]" id="inputEmail" placeholder="Email" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="span3">
                <label class="control-label" for="inputPassword">Contraseña</label>
                <div class="controls">
                    <input type="password" name="data[User][password]" id="inputPassword" placeholder="Password" />
                </div>
            </div>
            <div class="span3">
                <label class="control-label" for="inputConfirm">Confirmacion</label>
                <div class="controls">
                    <input type="password" id="inputConfirm" name="data[User][confirm]"  placeholder="Confirma contraseña" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="span3">
                <label class="control-label" for="inputActive">Activo</label>
                <div class="controls">
                    <input type="checkbox" name="data[User][active]" id="inputActive" />
                </div>
            </div>
        </div>
    </div>
    <div class="span9">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#">Grupo</a>
            </li>
        </ul>
        <div class="row">
            <div class="span9">
                <?php echo $this->Form->select('aro_id', $groups);?>
            </div>
        </div>
    </div>
    <div class="span9">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#">Permisos</a>
            </li>
        </ul>
        <div class="row permissions">
            <?php
            foreach($permissions as $moduleId => $moduleName):
            ?>
            <div class="span3">
                <input class="parent-permission" type="checkbox" value='<?php echo $moduleId; ?>' /> <?php echo $moduleName; ?>
                <div>
                    <input name="data[Permission][<?php echo $moduleName?>][read]" type="checkbox" value='1' /> read <br />
                    <input name="data[Permission][<?php echo $moduleName?>][update]" type="checkbox" value='1' /> update <br />
                    <input name="data[Permission][<?php echo $moduleName?>][delete]" type="checkbox" value='1' /> delete <br />
                </div>
            </div>
            <?php
            endforeach;
            ?>
        </div>
    </div>
</div>
<?php echo $this->Form->end();?>