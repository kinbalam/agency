<div class="row-fluid">
<div class="span10 offset1">

	<div class="row-fluid">
	    <div class="span6 text-center hero-unit">
	    	<h1>Sistema de Control de Agencia</h1>
	    	<p>Administra tu agencia de una manera f&aacute;cil y r&aacute;pida.</p>
	    </div>
	    <div class="span6">
	    	<br/><br/><br/>
	    	<h1 class="text-center">Men&uacute; R&aacute;pido</h1>
			<ul class="nav nav-tabs nav-stacked">
				<li><a href="<?=$this->Html->url(array('controller'=>'vehicles','action'=>'add','plugin'=>'pannel'))?>">Dar De Alta Veh&iacute;culo</a></li>
				<li><a href="<?=$this->Html->url(array('controller'=>'quotes','action'=>'add','plugin'=>'pannel'))?>">Crear Cotizaci&oacute;n</a></li>
				<li><a href="#">Crear Contrato</a></li>
				<li><a href="#">Pagar Mensualidad</a></li>
				<li><a href="#">Consultar Saldo</a></li>
			</ul>
	    </div>	   	
	</div>
	<hr/>
</div>
</div>