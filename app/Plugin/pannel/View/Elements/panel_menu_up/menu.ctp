<ul class="nav">
    <?php
    $menu = $this->Menu->getMenu();
    foreach($menu as $key=>$optionDef)
    {
        $class='';

        if($key==$viewData['menu']['active'])
            $class='class="active"';
        if($key != 'users'){
    ?>
            <ul class="nav nav-pills">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php echo $optionDef['text'];?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- dropdown menu links -->
                        <li><a href="<?php echo $this->Html->url('/pannel/'.$key.'/index')?>">Ver Lista</a></li>
                        <li><a href="<?php echo $this->Html->url('/pannel/'.$key.'/add');?>">Agregar</a></li>
                    </ul>
                </li>
            </ul>
        <!-- <li <?php echo $class;?>><a href="<?php echo $optionDef['url'];?>"><?php echo $optionDef['text'];?></a></li> -->
    <?php
        }
    }
    ?>
</ul>

<?php
if($showNav)
{
    ?>
    <ul class="nav nav-pills pull-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <?php echo $this->UserEntity->getName();?>
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <!-- dropdown menu links -->
                <li><a href="<?php echo $this->Html->url('/pannel/users/admin_profile')?>">Perfil</a></li>
                <li><a href="<?php echo $this->Html->url('/pannel/users/logout');?>">Cerrar Session</a></li>
            </ul>
        </li>
    </ul>
    <p class="navbar-text pull-right">
        Logged as
    </p>
<?php
}
?>
