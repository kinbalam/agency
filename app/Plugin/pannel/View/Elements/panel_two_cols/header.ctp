<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand" href="#">Panel</a>
            <div class="nav-collapse collapse">
                <?php
                if($showNav)
                {
                ?>
                <ul class="nav nav-pills pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <?php echo $this->UserEntity->getName();?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                        <!-- dropdown menu links -->
                            <li><a href="<?php echo $this->Html->url('/pannel/users/admin_profile')?>">Perfil</a></li>
                            <li><a href="<?php echo $this->Html->url('/pannel/users/logout');?>">Cerrar Session</a></li>
                        </ul>
                    </li>
                </ul>
                <p class="navbar-text pull-right">
                    Logged as
                </p>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Element('alert');?>