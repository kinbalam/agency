<ul class="nav nav-pills nav-stacked nav-custom">
    <?php
    $menu = $this->Menu->getMenu();
    foreach($menu as $key=>$optionDef)
    {
        $class='';
        if($key==$viewData['menu']['active'])
            $class='class="active"';
    ?>
    <li <?php echo $class;?>><a href="<?php echo $optionDef['url'];?>"><?php echo $optionDef['text'];?></a></li>
    <?php
    }
    ?>
</ul>
