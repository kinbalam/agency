<?php
$enabledTableIcon = false;
if($viewData['panel']['type']=='table')
{
    $enabledTableIcon = true;
}
?>
<div class="row">
    <div id="top-nav" class="span9 asoreToolbar">
        <?php
        $this->Breadcrumbs->addCrumb('Users');
        echo $this->Breadcrumbs->getCrumbList(array('separator'=>'/'));
        ?>
        <div class="btn-toolbar pull-right">
            <div class="btn-group pull-right">
                <button class="btn grid-view" <?php echo (!$enabledTableIcon==false)? '':' disabled="disabled"'; ?>><i class="icon-th"></i></button>
                <button class="btn table-view" <?php echo ($enabledTableIcon)? ' disabled="disabled"':''; ?>><i class="icon-align-justify"></i></button>
            </div>
            <div class="btn-group pull-right">
                <button class="btn"><i class="icon-search"></i></button>
            </div>
            <div class="btn-group">
            <?php
            if($this->UserEntity->hasPermission($module,'create'))
            {
            ?>
                <button class="btn add-action">Add</button>
            <?php
            }
            
            if($this->UserEntity->hasPermission($module,'update'))
            {
            ?>
                <button class="btn edit-action">Edit</button>
            <?php
            }
            
            if($this->UserEntity->hasPermission($module,'delete'))
            {
            ?>
                <button class="btn delete-action">Delete</button>
            <?php
            }
            ?>
            </div>
        </div>
        <?php
        echo $this->Form->create('viewConfig',array('id'=>'form-view'));
        echo $this->Form->hidden('rowSelected');
        echo $this->Form->hidden('viewType',array('id'=>'view-type'));
        echo $this->Form->end();
        ?>
    </div>
</div>