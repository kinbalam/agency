<?php
$columns = $viewData['panel']['columns'];

if(isset($records))
{
    foreach($records as $record)
    {
        $record = current($record);
?>
<tr>
    <td>
        <?php
        echo $this->Form->checkbox('checkOne', array('id' => $record['id']));
        ?>
    </td>
        <?php
        foreach($columns as $column=>$description)
        {
        ?>
    <td><?php echo $record[$description];?></td>
    <?php
        }
    ?>
</tr>
<?php
    }
}
?>