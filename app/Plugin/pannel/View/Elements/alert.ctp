<?php
if(isset($viewData) && $viewData['alert']['show'])
{
    $title   = '';
    $type    = $viewData['alert']['type'];
    $message = $viewData['alert']['message'];
    
    if(isset($viewData['alert']['title']))
        $title = $viewData['alert']['title'];
    
    if(empty($title))
        switch($type)
        {
            case 'success':
                $title = 'Enhorabuena';
                break;
        
            case 'error':
                $title = 'Verifica tus datos';
                break;
        
            case 'warning':
                $title = 'Cuiado.';
                break;
        
            default:
                $title = '';
                break;
        }
?>
<div id="system-message" class="alert alert-<?php echo $type;?>">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong><?php echo $title;?></strong>, <?php echo $message;?>
</div>
<?php
}
?>