<?php
$columns = $viewData['panel']['columns'];
?>
<table class="table asore-grid">
    <thead>
        <tr>
            <th>
                <?php
                echo $this->Form->checkbox('checkAll');
                ?>
            </th>
            <?php
            foreach($columns as $column=>$description)
            {
            ?>
            <th><?php echo $description;?></th>
            <?php
            }
            ?>
        </tr>
    </thead>
    <tbody id="records-container">
        <?php
        if(isset($records))
        {
            foreach($records as $data)
            {
                $record = current($data);
        ?>
        <tr>
            <td>
                <?php
                echo $this->Form->checkbox('checkOne');
                ?>
            </td>
                <?php
                foreach($columns as $column=>$description)
                {
                ?>
            <td><?php echo $record[$description];?></td>
            <?php
                }
            ?>
        </tr>
        <?php
            }
        }
        ?>
    </tbody>
    <tfoot>
        <tr>
            <td><input type="hidden" id="recordSelected" /></td>
        </tr>
    </tfoot>
</table>