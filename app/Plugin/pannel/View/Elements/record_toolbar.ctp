<div class="row">
    <div id="top-nav" class="span9 asoreToolbar">
        <?php
        $this->Breadcrumbs->addCrumb('Users','/pannel/users/admin_index');
        $this->Breadcrumbs->addCrumb($viewAlias);
        echo $this->Breadcrumbs->getCrumbList(array('separator'=>'/'));
        ?>
        <div class="btn-toolbar pull-right">
            <div class="btn-group">
            <?php
            if($this->UserEntity->hasPermission($module,'create') && ($viewAlias == 'add' || $viewAlias == 'edit'))
            {
            ?>
                <button class="btn save-action">Save</button>
            <?php
            }
            
            if($this->UserEntity->hasPermission($module,'update') && ($viewAlias == 'view'))
            {
            ?>
                <button class="btn edit-action">Edit</button>
            <?php
            }
            
            if($this->UserEntity->hasPermission($module,'delete') && ($viewAlias == 'edit'))
            {
            ?>
                <button class="btn delete-action">Delete</button>
            <?php
            }
            ?>
                <button class="btn cancel-action" onclick="document.location.href = 'admin_index'">Cancel</button>
            </div>
        </div>
    </div>
</div>