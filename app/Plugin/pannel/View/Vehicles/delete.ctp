<div class="row-fluid">
    <div class="span8 offset2 text-warning">
        <? $data =  $this->request->data;
            if (empty($data)): ?>
            <h3>No existe el veh&iacute;culo con folio seleccionado.</h3>
            <a class="btn btn-inverse inline" href="/panel/vehicles/index">Atras</a>

        <? else: ?>
            <h3>Confirmaci&oacute;n</h3>
        <? endif;?>
    </div>
</div>
<? if (!empty($data)):

    $data = $data['Vehicle'];
    ?>
<div class="row-fluid">
    <div class="span8 offset2 alert alert-error text-center">
        <div class="row-fluid">
             <div class="span12">
            ¿Realmente usted desea eliminar el veh&iacute;culo con folio <?=$data['id']?>?
             </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                Marca: <strong><?=$data['brand']?></strong>, Modelo <strong><?=$data['model']?></strong>,
                A&ntilde;o <strong><?=$data['year']?></strong>, Color <strong><?=$data['color']?></strong>
            </div>
        </div>

    </div>
</div>
<div class="row-fluid">
    <div class="span8 offset2 text-center">
        <div class="row-fluid">
            <div class="span6">
                <a class="btn btn-inverse inline" href="/pannel/vehicles/view/<?=$data['id']?>">Cancelar</a>
            </div>
            <div class="span6">
                <?= $this->Form->create('Vehicle', array('type'=>'delete','action'=>'delete')); ?>
                <button class="btn btn-danger">Eliminar</button>
                <?= $this->Form->end(); ?>
            </div>

        </div>

    </div>
</div>
<? endif; ?>