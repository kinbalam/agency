<div class="row-fluid">
    <div class="span12 hero-unit">


<div class="row-fluid">
    <div class="span12">
        <h3>Agregar Nuevo Veh&iacute;culo</h3>
    </div>
</div>

<?= $this->Form->create('Vehicle'); ?>

<?= $this->Form->hidden('activo',array('value'=>1)); ?>

<div class="row-fluid">

    <?= $this->Form->input('brand',array('div'=>'span4','label'=>'Marca')); ?>

    <?= $this->Form->input('model',array('div'=>'span4','label'=>'Modelo')); ?>

    <?= $this->Form->input('year',array('div'=>'span4','label'=>'A&ntilde;o')); ?>

</div>
<div class="row-fluid">


    <?= $this->Form->input('color',array('div'=>'span4')); ?>

    <?= $this->Form->input('no_serie',array('div'=>'span4')); ?>

    <?= $this->Form->input('no_motor',array('div'=>'span4')); ?>

</div>
<div class="row-fluid">


    <?= $this->Form->input('km',array('div'=>'span4')); ?>

    <?= $this->Form->submit('Guardar', array('div'=>'span8','class'=>'btn btn-large btn-primary'));  ?>

</div>

<?= $this->Form->end(); ?>

</div>
</div>
