<div class="row-fluid">
    <div class="span12 hero-unit">

<div class="row-fluid">
	<div class="span6">
		<h3>Lista de Veh&iacute;culos Activos</h3>
	</div>
    <div class="span6">
        <?= $this->Form->create(null,array('type'=>'post','style'=>'margin-top:15px;')) ?>
        <div class="row-fluid">
            <div class="span6">
                <?= $this->Form->input('q',array('placeholder'=>'folio/marca/modelo/año','label'=>false,'div'=>false,'class'=>'large-medium search-query'))?>
            </div>
            <div class="span6">
                <?= $this->Form->button('buscar',array('label'=>false,'div'=>false,'class'=>'btn','type'=>'submit'))?>
            </div>
        </div>
        <?=$this->Form->end()?>
    </div>
</div>

<?
    $message =  $this->Session->flash('good');
    if ($message):
?>
<div class="row-fluid">
    <div class="span8 offset2 alert alert-success text-center">
        <?=$message ?>
    </div>
</div>
<? endif; ?>

<?
$message =  $this->Session->flash('bad');
if ($message):
    ?>
    <div class="row-fluid">
        <div class="span8 offset2 alert alert-error text-center">
            <?=$message ?>
        </div>
    </div>
<? endif; ?>
<div class="row-fluid">
    <div class="span12">
       <?php echo $this->paginator->numbers(); ?>
    </div>
</div>
<div class="row-fluid">
    <table class="table table-hover">
        <thead>
        <th class="span1">Folio</th>
        <th class="span2">Marca</th>
        <th class="span2">Modelo</th>
        <th class="span2">A&ntilde;o</th>
        <th class="span2">Color</th>
        <!--
        <th>N. Serie</th>
        <th>N. Motor</th>-->
        <th class="span2">KM</th>
        <th class="span1">&nbsp;</th>

        </thead>
        <tbody>

        <?php foreach($data as $item): $item=$item['Vehicle'] ?>
            <tr>
                <td><?= $item['id']; ?> </td>
                <td><?= $item['brand']; ?> </td>
                <td><?= $item['model']; ?> </td>
                <td><?= $item['year']; ?> </td>
                <td><?= $item['color']; ?> </td>
                <!-- <td><?= $item['no_serie']; ?> </td>
                <td><?= $item['no_motor']; ?> </td>
                -->
                <td><?= number_format($item['km']); ?> </td>


                <td><a class="btn btn-mini btn-primary" href="/pannel/vehicles/view/<?=$item['id']?>">
                        <i class="icon-eye-open icon-white"></i> Ver
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>

    </table>
</div>
</div>
</div>