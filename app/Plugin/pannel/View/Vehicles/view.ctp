<div class="row-fluid">
    <div class="span10 offset1 hero-unit">

        <div class="row-fluid">
            <div class="span12">
                <h3>Detalles De Veh&iacute;culo Folio <?= $data['id'] ?></h3>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <h5>Creado el <?= $data['created'] ?></h5>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="span4">Marca</th>
                        <th class="span4">Modelo</th>
                        <th>A&ntilde;o</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?=$data['brand']?></td>
                        <td><?=$data['model']?></td>
                        <td><?=$data['year']?></td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="span2">KM</th>
                        <th class="span2">Color</th>
                        <th class="span2">No Serie</th>
                        <th class="span2">No Motor</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?=$data['km']?></td>
                        <td><?=$data['color']?></td>
                        <td><?=$data['no_serie']?></td>
                        <td><?=$data['no_motor']?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <a class="btn btn-danger" href="/pannel/vehicles/delete/<?=$data['id']?>">Eliminar</a>
            </div>

        </div>

    </div>
</div>