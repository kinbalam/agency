<?php
App::uses('AppHelper', 'View/Helper');
class UserEntityHelper extends AppHelper
{
    public $helpers = array('Session');
    public $components = array('Pannel.PannelAcl');
    
    protected $user = array();
    
    /**
     *
     * beforeRender.
     *
     * Helper callback is called after the controller’s beforeRender method
     * but before the controller renders view and layout.
     * 
     * @param string $view The file being rendered.
     *
     * @access public.
     * @return void.
     */
    public function beforeRender($view)
    {
        $this->user = $this->getUser();
    }
    
    /**
     *
     * getGroupDesc.
     *
     * 
     *
     * @access public.
     * @return void.
     */
    public function getGroupName()
    {
        return $this->user['group'];
    }
    
    
    /**
     *
     * getUser.
     *
     * Set the $user member from the Session variable that holds the current
     * logged user.
     *
     * @param void.
     *
     * @access private.
     * @return mixed/array
     */
    private function getUser()
    {
        $user = $this->Session->read('Auth.User');
        if(!empty($user))
            return $user;
        
        return array();
    }
    
    /**
     *
     * getName.
     *
     * Returns the name/description of the current logged use.
     *
     * @param void.
     * 
     * @access public.
     * @return string.
     */
    public function getName()
    {
        return $this->user['description'];
    }
    
    /**
     *
     * getId.
     *
     * Returns the id of the current logged user.
     *
     * @param void.
     * @access public.
     * @return int.
     */
    public function getId()
    {
        return $this->user['id'];
    }
    
    /**
     *
     * hasPermission.
     *
     * @param string $controller.
     * @param string $action.
     *
     * @access public.
     * @return bool.
     */
    public function hasPermission($controller,$action)
    {
        return PannelAclComponent::isAuthorized($controller,$action);
    }
}
?>