<?php
App::uses('AppHelper', 'View/Helper');
class MenuHelper extends AppHelper
{
    public $helpers = array('Html');
    /**
     *
     * getMenu.
     *
     * @param string $type.
     * @access public.
     * @return mixed/array.
     */
    public function getMenu($type = '')
    {
        switch($type)
        {
            case 'static':
                return $this->staticMenu();
                break;
            
            default:
                return $this->dynamicMenu();
                break;
        }
    }
    
    /**
     *
     * staticMenu.
     *
     * @param void.
     * @access public.
     * @return void.
     */
    public function staticMenu()
    {
        return array('dashboard'=>array('text'=>'Dashboard',
                                        'url'=>$this->Html->url('/panel/admin_dashboard')
                                        ),
                     'Users'=>array('text'=>'Perfil',
                                   'url'=>$this->Html->url('/users/admin_profile'))
                 );
    }
    
    /**
     *
     * dynamicMenu.
     *
     * @param void.
     * @access public.
     * @return void.
     */
    public function dynamicMenu()
    {
        $menu = array();
        $permissions = PannelAclComponent::getPermissions();
        
        if(!empty($permissions))
        {
            foreach($permissions as $controller=>$permission)
            {
                if(isset($permission['ismenu']) && $permission['ismenu']==1 && $permission['read']>=1)
                {
                    $menu[$controller] = array('text'=>$controller,
                                               'url'=>$this->Html->url('/pannel/'.$controller.'/index'));
                }
            }
        }
        
        return $menu;
    }
}
?>