<?php
App::uses('AppHelper', 'View/Helper');
class BreadcrumbsHelper extends AppHelper
{
    protected $_crumbs = array();
    public $helpers = array('Html');
    
    /**
     *
     * 
     *
     *
     *
     */
    public function addCrumb($name, $link = null, $options = null)
    {
        $this->_crumbs[] = array($name, $link, $options);
    }
    
    /**
     *
     * getCrumbList.
     *
     * @access public.
     * @return mixed/html.
     */
    public function getCrumbList($options)
    {
        $crumbCount = count($this->_crumbs);
        $separator  = $options['separator'];
        $result     = '';
        
        foreach($this->_crumbs as $which=>$crumb)
        {
            if(empty($crumb[1]))
            {
                $elementContent = $crumb[0];
            }else
            {
                $elementContent = $this->Html->link($crumb[0],$crumb[1],$crumb[2]); 
            }
            
            if (!empty($separator) && ($crumbCount - $which >= 2))
            {
	        $elementContent .= $this->Html->tag('span',$separator,array('class'=>'divider'));
            }
            
            $result .=$this->Html->tag('li', $elementContent);
        }
        
        return $this->Html->tag('ul',$result,array('class'=>'breadcrumb custom-breadcrumb pull-left'));
    }
}
?>