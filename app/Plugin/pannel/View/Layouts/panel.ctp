<!-- DOCTYPE FOR HTML 5. -->
<!DOCTYPE html>
<html>
    <head>
        <!--
            Character Encoding.
            * Server Config (Transpor layer | Headers).
            * UBOM. (At the begining of the File):
            * <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">  | Still allowed.
        -->
        <meta charset="UTF-8">
        <title>Sistema de Control de Agencia</title>
        <style>
            
        </style>
        <?php
        echo $this->Html->css('/Pannel/css/bootstrap.min.css'), 
             $this->Html->css('/Pannel/css/bootstrap-responsive.min.css'), 
             $this->Html->css('/Pannel/css/custom.css'),
             $this->Html->script('/Pannel/js/libs/jquery-1.9.1.js');

        ?>
    </head>
    <body>
        <?php
       // echo $this->Element('panel_two_cols/header',array('showNav'=>(isset($viewData) ? $viewData['nav']['show']:false)));
        ?>
        <div class="navbar-wrapper">
            <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
            <div class="container">

                <div class="navbar navbar-inverse">
                    <div class="navbar-inner">
                        <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
                        <button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <? echo $this->Html->link('SCA | Autos Durán',
                                array( 'plugin' => 'pannel', 'controller' => 'users', 'action' => 'index' ),
                                array('class'=>'brand')); ?>
                        <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
                        <div class="nav-collapse collapse" style="height: 0px;">
                            <!-- Example Begin -->
                            <!--
                            <ul class="nav">
                                <li class="active"><a href="#">Home</a></li>
                                <li><a href="#about">About</a></li>
                                <li><a href="#contact">Contact</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li class="nav-header">Nav header</li>
                                        <li><a href="#">Separated link</a></li>
                                        <li><a href="#">One more separated link</a></li>
                                    </ul>
                                </li>
                            </ul>
                            -->
                            <!-- Example Ends -->
                            <?php
                             echo $this->Element('panel_menu_up/menu',array('showNav'=>(isset($viewData) ? $viewData['nav']['show']:false)));
                            ?>
                        </div><!--/.nav-collapse -->
                    </div><!-- /.navbar-inner -->
                </div><!-- /.navbar -->

            </div> <!-- /.container -->
        </div>
        <div class="container">
            <div class="row">
                <?php
                $contentSpanClass = 'span12';
                ?>
                <div class="<?php echo $contentSpanClass;?>">
                    <?php
                    echo $content_for_layout;
                    ?>
                </div>
            </div>
        </div>
        <?php
        echo $this->Html->script('/pannel/js/libs/bootstrap.min.js');
        echo $this->Html->script('/pannel/js/libs/asore.framework.js');
        echo $this->Html->script('/pannel/js/libs/asore.toolbar.js');
        ?>
        <script type="text/javascript">
            var myApp = asore.createProduct();
            asore.createNamespace(myApp, 'toolbar');
            asore.createNamespace(myApp, 'general.labels');
            
            jQuery(document).ready(function()
            {
                myApp.general.controller = '<?php echo $module;?>';
            });
            
        </script>
        <?php
        echo $this->Html->script('/pannel/js/libs/asore.main.js'), 
             $this->Html->script('/pannel/js/libs/asore.pageExtend.js'), 
             $scripts_for_layout;
        ?>
    </body>
</html>
