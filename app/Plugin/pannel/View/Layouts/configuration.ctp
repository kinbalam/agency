<!-- DOCTYPE FOR HTML 5. -->
<!DOCTYPE html>
<html>
    <head>
        <!-- 
            Character Encoding.
            * Server Config (Transpor layer | Headers).
            * UBOM. (At the begining of the File):
            * <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">  | Still allowed.
        -->
        <meta charset="UTF-8">
        <title>HTML 5 Template</title>
        <?php
        echo $this->Html->css('/pannel/css/bootstrap.min.css');
        echo $this->Html->css('/pannel/css/bootstrap-responsive.min.css');
        echo $this->Html->css('/pannel/css/custom.css');
        ?>
    </head>
    <body>
        <div class="container">
            <div class="masthead">
                <h3 class="muted">
                <?php
                echo $this->Html->link('Pannel','/pannel/users/admin_index');
                echo ' > '.__d('pannel','CONFIGURATION_TITLE');?></h3>
            </div>
            <div class="wrapper">
                <?php echo $content_for_layout;?>
            </div>
        </div>
        <?php
        if(isset($debug) && $debug)
            echo $this->element('sql_dump');
        
        echo $this->Html->script('/pannel/js/libs/jquery-1.9.1.js');
        echo $this->Html->script('/pannel/js/libs/bootstrap.min.js');
        ?>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <?php echo $this->Html->script('/pannel/js/libs/html5shiv.js');?>
        <![endif]-->
        <script type="text/javascript">
            jQuery.noConflict();
        </script>
        <?php
        echo $scripts_for_layout;
        ?>
    </body>
</html>