<?php
echo $this->Form->create('Configuration');
?>
<h4><?php echo __d('pannel','CONFIGURATION_SYSTEM_ACCOUNT_TITLE');?></h4>
<?php
echo $this->Form->password('password',array('placeholder'=>__d('pannel','PASSWORD')));
echo $this->Form->password('confirm',array('placeholder'=>__d('pannel','PASSWORD_CONFIRM')));
echo $this->Form->button(__d('pannel','CONFIGURATION_SUBMIT'),array('class'=>'btn btn-large btn-primary'));
echo $this->Form->end();
?>
<p>
    <?php
    echo $message;
    ?>
</p>