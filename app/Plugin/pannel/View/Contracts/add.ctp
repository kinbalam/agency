<div class="row-fluid">
    <div class="span10 offset1 hero-unit">

<div class="row-fluid">
    <div class="span12">
        <h2>Nueva Cotizaci&oacute;n</h2>
    </div>
</div>

<?= $this->Form->create('Contract'); ?>
<?= $this->Form->hidden('active',array('value'=>1)); ?>

<div class="row-fluid">

    <div class="span6 box-client">

        <h5>Datos del Cliente</h5>

        <div class="row-fluid">
            <?= $this->Form->input('client_name',array('label'=>'Nombre del Cliente','div'=>'span12','class'=>'span12')); ?>
        </div>
        <div class="row-fluid">
            <?= $this->Form->input('client_address',array('label'=>'Dirección (Calle,Número,Cruzamientos,Colonia, CP)','div'=>'span12','class'=>'span12')); ?>
        </div>
        <div class="row-fluid">
            <?= $this->Form->input('client_city',array('label'=>'Ciudad','div'=>'span6','class'=>'span12')); ?>
            <?= $this->Form->input('client_state',array('label'=>'Estado','div'=>'span6','class'=>'span12')); ?>
        </div>
        <div class="row-fluid">
            <?= $this->Form->input('client_phone',array('label'=>'Teléfono','div'=>'span6','class'=>'span12')); ?>
            <?= $this->Form->input('client_mobile',array('label'=>'Celular','div'=>'span6','class'=>'span12')); ?>
        </div>
    </div>

    <div class="span6 box-client">

        <h5>Datos del Aval</h5>
        <div class="row-fluid">
            <?= $this->Form->input('client_name',array('label'=>'Nombre del Aval','div'=>'span12','class'=>'span12')); ?>
        </div>
        <div class="row-fluid">
            <?= $this->Form->input('client_address',array('label'=>'Dirección (Calle,Número,Cruzamientos,Colonia, CP)','div'=>'span12','class'=>'span12')); ?>
        </div>
        <div class="row-fluid">
            <?= $this->Form->input('client_city',array('label'=>'Ciudad','div'=>'span6','class'=>'span12')); ?>
            <?= $this->Form->input('client_state',array('label'=>'Estado','div'=>'span6','class'=>'span12')); ?>
        </div>
        <div class="row-fluid">
            <?= $this->Form->input('client_phone',array('label'=>'Teléfono','div'=>'span6','class'=>'span12')); ?>
            <?= $this->Form->input('client_mobile',array('label'=>'Celular','div'=>'span6','class'=>'span12')); ?>
        </div>
    </div>
</div>

<div class="row-fluid">
    <?= $this->Form->input('vehicle_id', array('label'=>'Selecciona un Vehículo','options' => $vehicles, 'empty'=>'Elige un vehículo','div'=>'span12','class'=>'span12','required'=>true));    ?>
</div>
<div class="row-fluid">
    <?= $this->Form->input('price',array('label'=>'Precio De Lista','between'=>'<span class="add-on">$</span>','div'=>'input-prepend span3','class'=>'span10','type'=>'text','default'=>0)); ?>
    <?= $this->Form->input('interest',array('label'=>'Interés Mensual','after'=>'<span class="add-on">%</span>','div'=>'input-append span3','class'=>'span5','type'=>'text','default'=>1.5)); ?>
    <?= $this->Form->input('tax',array('label'=>'IVA','after'=>'<span class="add-on">%</span>','div'=>'input-append span3','class'=>'span4','type'=>'text','default'=>16)); ?>
    <?= $this->Form->input('gc',array('label'=>'G.C.','after'=>'<span class="add-on">%</span>','div'=>'input-append span3','class'=>'span4','type'=>'text','default'=>150)); ?>

    
</div>
<div class="row-fluid">
    <?= $this->Form->input('premonths',array('div'=>'input-append span2','class'=>'span5','label'=>'Meses','value'=>48)); ?>
    <?= $this->Form->input('op',array('type'=>'radio','div'=>'span1','class'=>'span12','label'=>false,'value'=>'',  'options' => array('percentage' => ''), 'checked' => true)); ?>
    <?= $this->Form->input('percentage',array('after'=>'<span class="add-on">%</span>','div'=>'input-append span3','class'=>'span5','label'=>'Porcentaje de Enganche','value'=>20)); ?>
    <?= $this->Form->input('op',array('type'=>'radio','div'=>'span1','class'=>'span12','label'=>false,'value'=>'',  'options' => array('down_payment'=> ''))); ?>
    <?= $this->Form->input('down_payment',array('label'=>'Enganche','between'=>'<span class="add-on">$</span>','div'=>'input-prepend span3','class'=>'span10','type'=>'text','default'=>0)); ?>
    <?= $this->Form->input('balance',array('label'=>'Saldo','between'=>'<span class="add-on">$</span>','div'=>'input-prepend span2','class'=>'span10','readonly'=>true)); ?>
</div>

<div class="row-fluid">
    <div class="span12">
        <table class="table" style="display: none;">
            <thead>
            <th>Plazo</th>
            <th>Letra Adelantada</th>
            <th>Mensualidad</th>
            <th>Mensualidad Sin Inter&eacute;s</th>
            </thead>
            <tbody>
            <tr>
                <td><?= $this->Form->input('QuoteDet.0.months',array('type'=>'text','after' => ' <strong>Meses</strong>','class'=>'span3','label'=>false)); ?></td>
                <td><?= $this->Form->hidden('QuoteDet.0.capital_payment'); ?>
                    <?= $this->Form->input('QuoteDet.0.not_capital_payment',array('data-hidden'=>'QuoteDet0CapitalPayment','label'=>false,'class'=>'span8','disabled'=>true,'between'=>'<span class="add-on">$</span>','div'=>'input-prepend','type'=>'text','default'=>0)); ?> </td>
                <td><?= $this->Form->hidden('QuoteDet.0.normal_payment'); ?>
                    <?= $this->Form->input('QuoteDet.0.not_normal_payment',array('data-hidden'=>'QuoteDet0NormalPayment','label'=>false,'class'=>'span8','disabled'=>true,'between'=>'<span class="add-on">$</span>','div'=>'input-prepend','type'=>'text','default'=>0)); ?> </td>
                <td><?= $this->Form->hidden('QuoteDet.0.total_payment'); ?>
                    <?= $this->Form->input('QuoteDet.0.not_total_payment',array('data-hidden'=>'QuoteDet0TotalPayment','label'=>false,'class'=>'span8','disabled'=>true,'between'=>'<span class="add-on">$</span>','div'=>'input-prepend','type'=>'text','default'=>0)); ?> </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="row-fluid">
                        <button type="submit" class="btn btn-primary">
                            Guardar
                        </button>
                    </div>
                </td>
            </tr>
            </tbody>

        </table>

    </div>
</div>
<?= $this->Form->end();?>

    </div>
</div>
<script type="text/javascript" src="/pannel/js/contracts.js"></script>

