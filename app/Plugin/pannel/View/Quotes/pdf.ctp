<?
$pdf->AddPage();

$border=1;

$w = $pdf->w-$pdf->lMargin-$pdf->rMargin;

$pdf->Image($this->Html->url('/img/duran.png', true),$pdf->x,$pdf->y,30,0,'PNG');

$pdf->SetFont('Arial','B',24);
$pdf->x = $pdf->x + 35;
$pdf->Cell($w,8,utf8_decode('Autos Durán'),0,1);


$pdf->SetFont('Arial','',11);
$pdf->x = $pdf->x + 35;
$pdf->Cell($w,6,utf8_decode('Calle 5 diagonal x 40 esquina Num. 377 Col. Mérida.').$quote['Quote']['created'],0,1);
$pdf->x = $pdf->x + 35;
$pdf->Cell($w,6,utf8_decode('Tel: (999) 195 12 63'),0,1);
$pdf->x = $pdf->x + 35;
$pdf->Cell($w,6,utf8_decode('Horario: L-V de 9:00 a.m. - 8:00  p.m. / Sábado de 9:00 a.m. - 4:00 p.m.'),0,1);
$pdf->Ln();
$pdf->Line($pdf->x,$pdf->y,$pdf->w - $pdf->rMargin ,$pdf->y);
$pdf->Ln();

$pdf->SetFont('Arial','B',24);
$pdf->Cell($w,8,utf8_decode('Folio de Cotización ').$quote['Quote']['id'],0,1);
$pdf->SetFont('Arial','',11);

$pdf->Cell($w,6,utf8_decode('Fecha de Creación ').$quote['Quote']['created'],0,1);
$pdf->Cell($w,6,utf8_decode('Fecha de Impresión ').date('Y-m-d H:i:s'),0,1);

$pdf->Ln();

$pdf->SetFont('Arial','',17);
$pdf->Cell($w,12,'DATOS DEL CLIENTE',$border,1);

$pdf->SetFont('Arial','B',13);
$pdf->Cell($w/3,10,'Nombre Del Cliente',$border);
$pdf->SetFont('Arial','',13);
$pdf->Cell(2*$w/3,10,$quote['Quote']['client'],$border,1);
$pdf->SetFont('Arial','B',13);
$pdf->Cell($w/3,10,utf8_decode('Número Teléfonico'),$border);
$pdf->SetFont('Arial','',13);
$pdf->Cell(2*$w/3,10,$quote['Quote']['phone'],$border,1);
$pdf->SetFont('Arial','B',13);
$pdf->Cell($w/3,10,utf8_decode('Correo Electrónico'),$border);
$pdf->SetFont('Arial','',13);
$pdf->Cell(2*$w/3,10,$quote['Quote']['email'],$border,1);
$pdf->Ln();

$pdf->SetFont('Arial','',17);
$pdf->Cell($w,12,utf8_decode('DATOS DEL VEHÍCULO'),$border,1);
$pdf->SetFont('Arial','B',13);
$pdf->Cell($w/3,10,'Folio ',$border);
$pdf->SetFont('Arial','',13);
$pdf->Cell(2*$w/3,10,$quote['Vehicle']['id'],$border,1);
$pdf->SetFont('Arial','B',13);
$pdf->Cell($w/3,10,utf8_decode('Descripción'),$border);
$pdf->SetFont('Arial','',13);
$pdf->Cell(2*$w/3,10,$quote['Vehicle']['brand'].' '.$quote['Vehicle']['model'].' '.$quote['Vehicle']['year'],$border,1);
$pdf->Ln();


$pdf->SetFont('Arial','',17);
$pdf->Cell($w,12,utf8_decode('Descripción del Financiamiento'),$border,1);

$pdf->SetFont('Arial','B',13);
$pdf->Cell($w/5,10,'Precio de Lista ',$border);
$pdf->SetFont('Arial','',12);
$pdf->Cell($w/5,10,'$ '.number_format($quote['Quote']['price'],2).' MXN',$border);

$pdf->SetFont('Arial','B',13);
$pdf->Cell($w/5,10,'Enganche',$border);
$pdf->SetFont('Arial','',12);
$pdf->Cell(2*$w/5,10,'$ '.number_format($quote['Quote']['down_payment'],2).' MXN ('.number_format(100*$quote['Quote']['down_payment']/$quote['Quote']['price'],2).' %)',$border,1);


$pdf->SetFont('Arial','B',13);
$pdf->Cell($w/5,10,utf8_decode('Tasa de Interés'),$border);
$pdf->SetFont('Arial','',12);
$pdf->Cell($w/5,10,number_format($quote['Quote']['interest'],2).' %',$border,0);
$pdf->SetFont('Arial','B',13);
$pdf->Cell($w/5,10,utf8_decode('Impuesto'),$border);
$pdf->SetFont('Arial','',12);
$pdf->Cell(2*$w/5,10,number_format($quote['Quote']['tax'],2).' %',$border,1);



$pdf->SetFont('Arial','B',13);
$pdf->Cell($w/18,10,utf8_decode('#'),$border);
$pdf->Cell(2*$w/18,10,utf8_decode('Plazo'),$border);
$pdf->Cell(5/3*$w/6,10,utf8_decode('Letra Adelantada'),$border);
$pdf->Cell(5/3*$w/6,10,utf8_decode('Mensualidad'),$border);
$pdf->Cell(5/3*$w/6,10,utf8_decode('Mensualidad Con IVA'),$border,1);


$pdf->SetFont('Arial','',10);
foreach($quote['QuoteDet'] as $key=>$quote_det){
    $pdf->Cell($w/18,10,$key+1,$border);
    $pdf->Cell(2*$w/18,10,$quote_det['months'].' meses',$border);
    $pdf->Cell(5/3*$w/6,10,'$ '.number_format($quote_det['capital_payment'],2).' MXN',$border);
    $pdf->Cell(5/3*$w/6,10,'$ '.number_format($quote_det['normal_payment'],2).' MXN',$border);
    $pdf->Cell(5/3*$w/6,10,'$ '.number_format($quote_det['total_payment'],2).' MXN',$border,1);
}

$pdf->Output(); ?>