<div class="row-fluid">
    <div class="span12 hero-unit">

<div class="row-fluid">
	<div class="span6">
		<h3>Lista de Cotizaciones</h3>
	</div>
    <div class="span6">
        <?= $this->Form->create('Quote',array('type'=>'post','style'=>'margin-top:15px;')) ?>
        <div class="row-fluid">
            <div class="span6">
                <?= $this->Form->input('q',array('placeholder'=>'cliente/email/marca/modelo','label'=>false,'div'=>false,'class'=>'large-medium search-query'))?>
            </div>
            <div class="span6">
                <?= $this->Form->button('buscar',array('label'=>false,'div'=>false,'class'=>'btn','type'=>'submit'))?>
            </div>
        </div>
        <?=$this->Form->end()?>
    </div>
</div>

<?
    $message =  $this->Session->flash('good');
    if ($message):
?>
<div class="row-fluid">
    <div class="span8 offset2 alert alert-success text-center">
        <?=$message ?>
    </div>
</div>
<? endif; ?>

<?
$message =  $this->Session->flash('bad');
if ($message):
    ?>
    <div class="row-fluid">
        <div class="span8 offset2 alert alert-error text-center">
            <?=$message ?>
        </div>
    </div>
<? endif; ?>
<div class="row-fluid">
    <div class="span12">
       <?php echo $this->paginator->numbers(); ?>
    </div>
</div>
<div class="row-fluid">
    <table class="table table-hover">
        <thead>
        <th class="span1">Folio</th>
        <th class="span2">Cliente</th>
        <th class="span2">Tel&eacute;fono</th>
        <th class="span2">Email</th>
        <th class="span2">Veh&iacute;culo</th>
        <th class="span2">Fecha de Creaci&oacute;n</th>
        <th class="span1">&nbsp;</th>

        </thead>
        <tbody>

        <?php foreach($data as $item): $quote=$item['Quote']; $vehicle = $item['Vehicle'] ?>
            <tr>
                <td><?= $quote['id']; ?> </td>
                <td><?= $quote['client']; ?> </td>
                <td><?= $quote['phone']; ?> </td>
                <td><?= $quote['email']; ?> </td>
                <td><?= $vehicle['descrip']; ?> </td>
                <td><?= $vehicle['created']; ?> </td>
                <td><a class="btn btn-mini btn-primary" href="/pannel/quotes/view/<?=$quote['id']?>">
                        <i class="icon-eye-open icon-white"></i> Ver
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>

    </table>
</div>

</div>
</div>

