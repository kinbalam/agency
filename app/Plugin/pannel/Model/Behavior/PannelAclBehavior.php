<?php
App::uses('AclBehavior', 'Model/Behavior');
class PannelAclBehavior extends AclBehavior
{
    public function setup(Model $model, $config = array()) {
        if (isset($config[0])) {
            $config['type'] = $config[0];
            unset($config[0]);
        }
        $this->settings[$model->name] = array_merge(array('type' => 'controlled'), $config);
        $this->settings[$model->name]['type'] = strtolower($this->settings[$model->name]['type']);

        $types = $this->_typeMaps[$this->settings[$model->name]['type']];

        if (!is_array($types)) {
            $types = array($types);
        }
        
        foreach ($types as $type) {
            $model->{$type} = ClassRegistry::init($type);
        }
    }
    
    /**
     *
     * parentNode.
     *
     * 
     *
     * @param string $type.
     * @access public.
     * @return string.
     */
    public function parentNode($type)
    {
        $alias = '';
        if(isset($this->model->data))
        {
            if(isset($this->model->data[$this->model->name][strtolower($type).'_id']))
            {
                $alias = $this->getTypeAlias($this->model->data[$this->model->name][strtolower($type).'_id'],$type);
            }
        }
        
        return $alias;
    }
    
    /**
     *
     * getTypeAlias.
     *
     * Gets the aro alias from the aro_id passed to it.
     *
     * @param string $aro_id aro id.
     * @access public.
     * @return string.
     */
    public function getTypeAlias($id,$type)
    {
        $this->model->{$type}->id = $id;
        $aclResult = $this->model->{$type}->read('alias');
        return $aclResult[$type]['alias'];
    }
    
    /**
    * Creates a new ARO/ACO node bound to this record
    *
    * @param Model $model
    * @param boolean $created True if this is a new record
    * @param array $options Options passed from Model::save().
    * @return void
    */
    public function afterSave(Model $model, $created, $options = array())
    {
        $useParentNodeBase = false;
        $this->model = $model;
        
        $types = $this->_typeMaps[$this->settings[$model->name]['type']];
        if (!is_array($types)) {
            $types = array($types);
        }
        
        if (!method_exists($model, 'parentNode'))
        {
            $useParentNodeBase = true;
        }
        
        foreach ($types as $type)
        {
            if(!$useParentNodeBase)
            {
                $parent = $model->parentNode();
            }else
            {
                $parent = $this->parentNode($type);
            }
            
            if (!empty($parent)) {
                    $parent = $this->node($model, $parent, $type);
            }
            $data = array(
                    'parent_id' => isset($parent[0][$type]['id']) ? $parent[0][$type]['id'] : null,
                    'model' => $model->name,
                    'foreign_key' => $model->id,
                    'alias'=> isset($model->data[$model->name]['alias']) ? $model->data[$model->name]['alias'] : ' '
            );
            if (!$created) {
                    $node = $this->node($model, null, $type);
                    $data['id'] = isset($node[0][$type]['id']) ? $node[0][$type]['id'] : null;
            }
            $model->{$type}->create();
            $model->{$type}->save($data);
        }
    }
}
?>