<?php

class QuoteDet extends AppModel {

	public $validate = array(
        'months' => array(
            'rule'    => 'notEmpty',
            'message' => 'Este campo no puede estar vacío',
        ),
        'capital_payment' => array(
            'rule'    => 'notEmpty',
            'message' => 'Este campo no puede estar vacío',
        ),
        'normal_payment' => array(
            'rule'    => 'notEmpty',
            'message' => 'Este campo no puede estar vacío',
        ),
        'total_payment' => array(
            'rule'    => 'notEmpty',
            'message' => 'Este campo no puede estar vacío',
        ),
        'taxes' => array(
            'rule'    => 'notEmpty',
            'message' => 'Este campo no puede estar vacío',
        ),
    )

}
