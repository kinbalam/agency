<?php
/**
 * Created by JetBrains PhpStorm.
 * User: miguel
 * Date: 01/08/13
 * Time: 18:33
 * To change this template use File | Settings | File Templates.
 */

class Quote extends PannelAppModel {

    public $belongsTo =  'Vehicle';

    public $hasMany = array(
        'QuoteDet' => array(
            'className' => 'QuoteDet',
            'order' => 'QuoteDet.months ASC',
            'dependent' => true
        )
    );

    public $validate = array(
        'client' => array(
            'rule'    => 'notEmpty',
            'message' => 'Este campo no puede estar vacío',
        ),
        'phone' => array(
            'rule'    => 'notEmpty',
            'message' => 'Este campo no puede estar vacío',
        ),
        'email' => array(
            'rule'    => 'notEmpty',
            'message' => 'Este campo no puede estar vacío',
        ),
        'vehicle_id' => array(
            'rule'    => 'notEmpty',
            'message' => 'Este campo no puede estar vacío',
        ),
        'price' => array(
            'rule'    => 'notEmpty',
            'message' => 'Este campo no puede estar vacío',
        ),
        'downpayment' => array(
            'rule'    => 'notEmpty',
            'message' => 'Este campo no puede estar vacío',
        ),
        'interest' => array(
            'rule'    => 'notEmpty',
            'message' => 'Este campo no puede estar vacío',
        ),
        'tax' => array(
            'rule'    => 'notEmpty',
            'message' => 'Este campo no puede estar vacío',
        ),
        'active' => array(
            'rule'    => 'notEmpty',
            'message' => 'Este campo no puede estar vacío',
        )
    );

}