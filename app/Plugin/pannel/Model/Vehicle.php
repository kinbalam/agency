<?php

class Vehicle extends PannelAppModel {

    public $virtualFields = array(
        'descrip' => 'CONCAT(Vehicle.brand, " ", Vehicle.model,". ",Vehicle.year,". ",Vehicle.color)'
    );

    public $validate = array(
        'brand' => array(
            'rule'    => array('maxLength', 255),
            'message' => 'Este campo no puede estar vac&iacute;o',
        ),
        'model' => array(
            'rule'    => array('maxLength', 255),
            'message' => 'Este campo no puede estar vac&iacute;o',
        ),
        'year' => array(
            'rule1'=> array(
                'rule'    => 'notEmpty',
                'message' => 'Este campo no puede estar vacío',
            ),
            'rule2'=>array(
                'rule'    =>  array('range', 1, 2100),
                'message' => 'Debe ser menor que 2100',
            )
        ),
        'color' => array(
            'rule'    => array('maxLength', 100),
            'message' => 'Este campo no puede estar vac&iacute;o',
        ),
        'no_serie' => array(
            'rule'    => array('maxLength', 100),
            'message' => 'Este campo no puede estar vac&iacute;o',
        ),
        'no_motor' => array(
            'rule'    => array('maxLength', 100),
            'message' => 'Este campo no puede estar vacío',
        ),
        'km' => array(
            'rule1'=> array(
                'rule'    => 'notEmpty',
                'message' => 'Este campo no puede estar vacío',
            ),
            'rule2'=>array(
                'rule'    =>  array('range', 1, 2147483647),
                'message' => 'Debe ser menor que 2,147,483,647 ',
            )
        )
    );

}