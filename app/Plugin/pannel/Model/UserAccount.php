<?php
class UserAccount extends PannelAppModel
{
    public $name     = 'UserAccount';
    public $useTable = 'users';
    public $actsAs   = array('Pannel.PannelAcl' => array('type' => 'requester'));
    
    /**
     *
     * parentNode.
     *
     * Necessary for the Acl Behaviour.
     *
     * @param void.
     * @access public.
     * @return string.
     */
    public function parentNode()
    {
        $alias = '';
        if(isset($this->data))
        {
            if(isset($this->data[$this->name]['aro_id']))
            {
                $alias = $this->getAroAlias($this->data[$this->name]['aro_id']);
            }
        }
        
        return $alias;
    }

    /**
     *
     * getAroAlias.
     *
     * Gets the aro alias from the aro_id passed to it.
     *
     * @param string $aro_id aro id.
     * @access public.
     * @return string.
     */
    public function getAroAlias($aro_id)
    {
        $this->Aro->id = $aro_id;
        $aro = $this->Aro->read('alias');
        return $aro['Aro']['alias'];
    }
}
?>