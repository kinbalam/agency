Asore Pannel
=======

[![Asore Pannel](http://asore.mx/images/icon-gears.png)](http://www.asore.mx)

Asore Pannel is a development effort for creating a CakePHP plugin that offers an administration module
for websites.

Todo
----------------
* Improve permissions loading.
* Create, Delete, Update Users.
* Support for submenus.


Installation
----------------
* Place plugin folder in your base app/Plugin folder.
* Load the plugin in your app/Config/bootstrap.php.
      CakePlugin::load('Pannel',array('routes'=>true));
* Change Acl.classname configuration in your app/core.php.
      * Configure::write('Acl.classname', 'PannelDbAcl'); 
       
* Run Config/Schema/db_acl.sql.
* Run Config/Schema/db_panel_data.sql.
* Navigate to /pannel/configurations/ in order to configure the system administrator.

Some Handy Links
----------------
[CakePHP](http://www.cakephp.org) - The rapid development PHP framework



